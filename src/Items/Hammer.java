
package Items;

import Atmosphere.WallSmashTwo;
import Atmosphere.Wallbreaking;
import MainInit.Factory;
import MainInit.Particles;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.Ray;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;


public class Hammer {
    private static Node wall;
    private static BulletAppState bulletAppState;
    private static float distance;
    private static AssetManager assetManager;
    private static int count = 0;
    
    public static void setup(Node wall)
    {
        Hammer.wall = wall;
    }
    
    public static void setup2(BulletAppState bulletAppState, AssetManager assetManager)
    {
        Hammer.bulletAppState = bulletAppState;
        Hammer.assetManager = assetManager;
    }
    
    public static boolean hammerCast(Camera cam)
    {
        CollisionResults results = new CollisionResults();
        
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        wall.collideWith(ray, results);

        if(results.getClosestCollision() != null)
        {
            distance = results.getClosestCollision().getDistance();
            Node parent = results.getClosestCollision().getGeometry().getParent();
            if(distance <= 10 && count == 0)
            {
                Material wallText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
                wallText = Factory.randomWallTexture(wallText);
                parent.setMaterial(wallText);

                Wallbreaking walBreakOne = new Wallbreaking(wall, assetManager);
                parent.attachChild(Particles.wallBreak());

                parent.attachChild(Particles.wallBreak());

                count++;
                return false;
            }
            else if(distance <= 10 && count == 1)
            {
               RigidBodyControl solid = parent.getControl(RigidBodyControl.class);
               WallSmashTwo wallSoundTwo = new WallSmashTwo(wall, assetManager);
               bulletAppState.getPhysicsSpace().remove(solid);
               parent.removeControl(RigidBodyControl.class);
               parent.detachAllChildren();
               
               count = 0;
               return true;
            }
        } 
        return false;
    }
    
}
