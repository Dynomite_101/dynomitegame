package Items;

import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import Level.IceStaging;

public class TorchPlacing {
    
    private static float distance;
    private static Node wall;
    
    public static void setup(Node wall)
    {
        TorchPlacing.wall = wall;
    }
    
    public static boolean torchCast(Camera cam)
    {
        {
        CollisionResults results = new CollisionResults();
        
            Ray ray = new Ray(cam.getLocation(), cam.getDirection());
            wall.collideWith(ray, results);
              
            if(results.getClosestCollision() != null) {
                distance = results.getClosestCollision().getDistance();
                if(distance <= 10)
                {
                   Node parent = results.getClosestCollision().getGeometry().getParent();
                   IceStaging.Torch(parent);
                   return true;
                }
            }
        } 
        return false;
    }
}
