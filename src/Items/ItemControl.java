package Items;

import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.GhostControl;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class ItemControl extends GhostControl {

    private float oneUpMoveControl = 0;
    private Spatial item;
    private Vector3f location;
    
    public ItemControl(CollisionShape shape, Spatial item) {
        super(shape);
        this.item = item;
        location = item.getLocalTranslation();
    }
    
    @Override
    public void update(float tpf) {
        super.update(tpf);
        floatAround(tpf);
    }
    
    private void floatAround(float tpf) {
        oneUpMoveControl += tpf;
        item.setLocalTranslation(location.addLocal(0f, FastMath.sin(oneUpMoveControl) * .001f, 0f));
        item.rotate(0f, tpf, 0f);
    }
    
}
