 package MainInit;

import Atmosphere.DeathSound;
import Level.*;
import Player.ItemRayCast;
import Player.Player;
import com.jme3.app.SimpleApplication;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import de.lessvoid.nifty.Nifty;
import enemyAI.EnemySpawner;
import Level.layout.Ice;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.system.AppSettings;
import java.util.ArrayList;
import java.util.List;

public class Main extends SimpleApplication implements ActionListener {

    private BulletAppState bulletAppState;
    private CharacterControl playerControl;
    private Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false, jump = false, start = false, fall = false, hammer = false;
    ScreenControl s;
    private boolean flyCamControl = true;
    private boolean menuControl = true;
    private boolean partyButton = false;
    private static float castTimer;
    private static float castCoolDown = 0.5f;
    private int health = 3;
    private qteAppState qte;
    private Nifty nifty;
    private Vector3f playerPosition;
    private Vector3f enemyPosition;
    public static boolean distanceCheck = true;
    private float distance;
    private Node worldNode = new Node("World");
    public static Node player;    
    private float playerWalkSpeed = (1f / 1.4f);
    public static boolean ableToBuy = false, newGame = false, finishedGame = false;
    private int loseAmount = 10;
    private Vector3f playerStartLocation = new Vector3f(-20, 5, 20);

    public static void main(String[] args) {
        AppSettings settings = new AppSettings(true);
        //settings.setResolution(640,480);
        settings.setResolution(1280,720);
        //settings.setFullscreen(true);
        
        Main app = new Main();
        app.showSettings = false;
        app.setSettings(settings);
        app.start();
        app.setDisplayStatView(false);
        app.setDisplayFps(false);
    }

    @Override
    public void simpleInitApp() {
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        flyCam.setMoveSpeed(100);
        //bulletAppState.setDebugEnabled(true);
        rootNode.attachChild(worldNode);

        AmbientLight bright = new AmbientLight();
        bright.setColor(ColorRGBA.White.mult(2));
        worldNode.addLight(bright);
        
        Setup.setup(assetManager, bulletAppState, worldNode, viewPort, cam);

        Level.layout.DefaultLayout layout = new Level.layout.Ice();
        layout.newLayout();
        layout.createLayout();
        Utilities.setup(layout.getLayout());
        Level.Staging staging = new Level.IceStaging();
        staging.setup(rootNode, bulletAppState);
        Level.Create level = new Level.Create();
        worldNode.attachChild(level.createLevel(staging, layout.getLayout(), bulletAppState));

        enemyAI.EnemySpawner ES = new EnemySpawner(layout.getLayout(), bulletAppState);
        worldNode.attachChild(ES.spawnEnemies());
        
        Setup.create();

        player = new Node("Player");
        playerControl = Player.initPlayer(playerControl, bulletAppState, rootNode, assetManager);
        player.setLocalTranslation(playerStartLocation);
        player.addControl(playerControl);
        worldNode.attachChild(player);

        s = new ScreenControl(this, cam);
        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(
                assetManager, inputManager, audioRenderer, guiViewPort);
        nifty = niftyDisplay.getNifty();
        nifty.fromXml("Interface/Gui.xml", "start", s);
        guiViewPort.addProcessor(niftyDisplay);

        flyCam.setDragToRotate(flyCamControl);
        stateManager.attach(s);

        qte = new qteAppState(assetManager, inputManager, audioRenderer, guiViewPort, stateManager, worldNode);

        niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
        nifty = niftyDisplay.getNifty();
        guiViewPort.addProcessor(niftyDisplay);

        setUpKeys();
    }

    public void newGame() {
        newGame = false;
        ArrayList<String> objects = new ArrayList<String>();
        objects.add("FloorNode");
        objects.add("PartitionNode");
        objects.add("UnbreakableWallNode");
        objects.add("BreakableWallNode");
        objects.add("IceSpikeNode");
        objects.add("Money");
        objects.add("OneUp");
        objects.add("EnemyNode");
        objects.add("pointLight");
        objects.add("Level");
        objects.add("Boss");
        objects.add("BossNode");
        objects.add("skeleton");
        objects.add("Key3Node");
        
        destroyObjects(objects);
        
        if (Player.keyFind == false) {
            Create.numberOfKey3 = 0;
        }

        Level.layout.DefaultLayout layout = new Level.layout.Ice();
        layout.newLayout();
        layout.createLayout();
        Level.Staging staging = new Level.IceStaging();
        staging.setup(rootNode, bulletAppState);
        Utilities.setup(layout.getLayout());
        Level.Create level = new Level.Create();
        worldNode.attachChild(level.createLevel(staging, layout.getLayout(), bulletAppState));

        player.removeControl(playerControl);
        player.setLocalTranslation(playerStartLocation);
        if(Player.getLives() == 0) {
            Player.addLife(3);
        }
        player.addControl(playerControl);
        if (finishedGame) {
            Player.keyBuy = false;
            Player.keyKill = false;
            Player.keyFind = false;
            finishedGame = false;
        }

        enemyAI.EnemySpawner ES = new EnemySpawner(layout.getLayout(), bulletAppState);
        worldNode.attachChild(ES.spawnEnemies());
        
        distanceCheck = true;
        Boss.create(worldNode);
    }

    private void destroyObjects(List<String> destroyables) {
        for (String toBeDestroyed : destroyables) {
            List<Spatial> destroyList = Utilities.findAll(toBeDestroyed, rootNode);
            for (Spatial object : destroyList) {
                object.removeFromParent();
                try {
                    bulletAppState.getPhysicsSpace().remove(object);
                } catch (NullPointerException npe) {
                    player.getLocalTranslation();
                }
            }
        }
    }

    private void setUpKeys() {
        inputManager.deleteMapping(SimpleApplication.INPUT_MAPPING_EXIT);
        inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_ESCAPE));
        inputManager.addMapping("Party", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("Shop", new KeyTrigger(KeyInput.KEY_B));
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Start", new KeyTrigger(KeyInput.KEY_Q));
        inputManager.addMapping("Fall", new KeyTrigger(KeyInput.KEY_F));
        inputManager.addMapping("Hammer", new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        inputManager.addMapping("Torch", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addListener(this, "Left", "Right", "Up", "Down");
        inputManager.addListener(actionListener, "Pause", "Party", "Shop", "Hammer", "Torch");
    }

    public void onAction(String name, boolean isPressed, float tpf) {
        if (menuControl == false) {
            if (name.equals("Left")) {
                left = isPressed;
            }
            if (name.equals("Right")) {
                right = isPressed;
            }
            if (name.equals("Up")) {
                up = isPressed;
            }
            if (name.equals("Down")) {
                down = isPressed;
            }
            if (name.equals("Jump")) {
                jump = isPressed;
            }
            if (name.equals("Start")) {
                start = isPressed;
            }
            if (name.equals("Fall")) {
                fall = isPressed;
            }
            if (name.equals("Hammer")) {
                hammer = isPressed;
            }
        }
    }
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("Pause") && !keyPressed) {
                if (menuControl == false) {
                    s.pauseGame();
                }
            }
            if (name.equals("Shop") && !keyPressed) {
                if (menuControl == false) {
                    s.shop();
                }
            }
            if (name.equals("Hammer") && !keyPressed) {
                if (menuControl == false) {
                    s.useHammer();
                }
            }
            if (name.equals("Torch") && !keyPressed) {
                if (menuControl == false) {
                    s.useTorch();
                }
            }
            if (name.equals("Party") && keyPressed) {
                partyButton = true;
            } else {
                partyButton = false;
                viewPort.setBackgroundColor(ColorRGBA.Black);
            }
        }
    };

    @Override
    public void simpleUpdate(float tpf) {
        Vector3f camDir = cam.getDirection().mult(0.6f);
        camDir.setY(0);
        Vector3f camLeft = cam.getLeft().mult(0.4f);
        walkDirection.set(0, 0, 0);
        if (left) {
            walkDirection.addLocal(camLeft);
        }
        if (right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if (up) {
            walkDirection.addLocal(camDir);
        }
        if (down) {
            walkDirection.addLocal(camDir.negate());
        }
        if (jump) {
            playerControl.setPhysicsLocation(new Vector3f(85, 150, 85));
            playerControl.setGravity(0);
            playerControl.setFallSpeed(0);
        }
        if (fall) {
            playerControl.setGravity(30);
            playerControl.setFallSpeed(30);
        }
        if (start) {
            playerControl.setPhysicsLocation(new Vector3f(4, 5, 4));
            playerControl.setGravity(30);
            playerControl.setFallSpeed(30);
        }
        playerControl.setWalkDirection(walkDirection.mult(playerWalkSpeed));
        cam.setLocation(playerControl.getPhysicsLocation());
        if (partyButton) {
            viewPort.setBackgroundColor(ColorRGBA.randomColor());
        }
        if (menuControl == false) {
            String Cast = ItemRayCast.autoCast();
            castTimer += tpf;
            if (castTimer >= castCoolDown) {
                s.checkCast(Cast);
                castTimer = 0;
            }
        }
        enemyPosition = Boss.model.getWorldTranslation();
        playerPosition = playerControl.getPhysicsLocation(); //enemy Distance to play chunck
        distance = playerPosition.distance(enemyPosition);
        
        if (distanceCheck == true) {
            if (distance < 10) {
                distanceCheck = false;
                Boss.animChannel.setAnim("Dance");
                stateManager.attach(qte);
            }
        }


        if (Player.getLives() <= 0) {
            newGame();
            Player.spendMoney(loseAmount);
            if (Player.getMoney() < 0) {
                this.stop();
            }
            loseAmount *= 2;
            DeathSound deathsound = new DeathSound(rootNode, assetManager);
        }
        if (newGame) {
            finishedGame=true;
            newGame();
        }
    }

    public void setFlyCamControl(boolean flyCamControl) {
        this.flyCamControl = flyCamControl;
        flyCam.setDragToRotate(flyCamControl);
    }

    public void setMenuControl(boolean menuControl) {
        this.menuControl = menuControl;
    }

    public Nifty getNifty() {
        return nifty;
    }
    //3090
}
