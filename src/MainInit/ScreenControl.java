package MainInit;

import Items.Hammer;
import Items.TorchPlacing;
import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.renderer.Camera;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class ScreenControl extends AbstractAppState implements ScreenController {

    private Nifty nifty;
    private Screen screen;
    private SimpleApplication app;
    private Main main;
    public Camera cam;
    private int money = 0;
    private int key = 0;
    private int torch = 0;
    private int hammer = 1;
    private int life = 3;
    private boolean hammerCheck;
    private boolean torchCheck;
    private int hammerCount = 0;
    private String castResult = " ";
    //Prices
    private int torchPrice = 20;
    private int hammerPrice = 75;
    private int lifePrice = 50;
    private int keyPrice = 500;

    public ScreenControl(Main main, Camera cam) {
        this.main = main;
        this.cam = cam;
    }

    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;

    }

    public void onStartScreen() {
    }

    public void onEndScreen() {
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        this.app = (SimpleApplication) app;
    }

    @Override
    public void update(float tpf) {
        this.money = Player.Player.getMoney();
        this.life = Player.Player.getLives();
    }

    public void startGame(String nextScreen) {
        main.setFlyCamControl(false);
        main.setMenuControl(false);
        nifty.gotoScreen(nextScreen);
    }

    public void quitGame() {
        app.stop();
    }

    public void continueGame() {
        main.setFlyCamControl(false);
        main.setMenuControl(false);
        nifty.fromXml("Interface/Gui.xml", "hud");
    }

    public void restartGame() {
        main.newGame();
        continueGame();
    }

    public void pauseGame() {
        main.setFlyCamControl(true);
        main.setMenuControl(true);
        nifty.fromXml("Interface/Gui.xml", "pause");
    }

    public void shop() {
        if (main.ableToBuy) {
            main.setFlyCamControl(true);
            main.setMenuControl(true);
            nifty.fromXml("Interface/Gui.xml", "shop");
        }
    }

    public int getMoney() {
        return money;
    }

    public int getKey() {
        return key;
    }

    public int getTorch() {
        return torch;
    }

    public int getHammer() {
        return hammer;
    }

    public int getLife() {
        return life;
    }

    public String getCastResult() {
        return castResult;
    }

    public void buyTorch() {
        //Max of 3 at a time
        if ((money >= torchPrice) && (torch < 3)) {
            Player.Player.spendMoney(torchPrice);
            torch = torch + 1;
            money = Player.Player.getMoney();
            nifty.fromXml("Interface/Gui.xml", "shop");
        }
    }

    public void buyHammer() {
        //Max of 3 throughtout the whole game
        if ((money >= hammerPrice) && (hammerCount < 3)) {
            Player.Player.spendMoney(hammerPrice);
            hammer = hammer + 1;
            money = Player.Player.getMoney();
            nifty.fromXml("Interface/Gui.xml", "shop");
            hammerCount++;
        }
    }

    public void buyKey() {
        //Can only be bought once
        if ((money >= keyPrice) && (Player.Player.keyBuy == false)) {
            Player.Player.spendMoney(keyPrice);
            key = key + 1;
            money = Player.Player.getMoney();
            nifty.fromXml("Interface/Gui.xml", "shop");
            Player.Player.keyBuy = true;
        }
    }

    public void buyLife() {
        //Max of 3 at a time
        if ((money >= lifePrice) && (life < 3)) {
            Player.Player.spendMoney(lifePrice);
            Player.Player.addLife(1);
            money = Player.Player.getMoney();
            nifty.fromXml("Interface/Gui.xml", "shop");
        }
    }


    public void checkCast(String castRes) {
        this.castResult = castRes;
        continueGame();
    }

    public void useHammer() {
        if (hammer > 0) {
            hammerCheck = Hammer.hammerCast(cam);
            if (hammerCheck == true) {
                hammer--;
            }
            hammerCheck = false;
        }
    }

    public void useTorch() {
        if (torch > 0) {
            torchCheck = TorchPlacing.torchCast(cam);
            if (torchCheck == true) {
                torch--;
            }
            torchCheck = false;
        }
    }
}
