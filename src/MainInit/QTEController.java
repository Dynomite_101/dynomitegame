package MainInit;

import com.jme3.app.state.AbstractAppState;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;
import de.lessvoid.nifty.render.NiftyImage;
import de.lessvoid.nifty.render.NiftyRenderEngine;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public class QTEController extends AbstractAppState implements ScreenController {

    private Element upArrow, downArrow, leftArrow, rightArrow;
    private NiftyImage arrowWhite, arrowRed;
    private Nifty nifty;
    private Screen screen;

    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
        upArrow = nifty.getCurrentScreen().findElementByName("upArrow");
        rightArrow = nifty.getCurrentScreen().findElementByName("rightArrow");
        leftArrow = nifty.getCurrentScreen().findElementByName("leftArrow");
        downArrow = nifty.getCurrentScreen().findElementByName("downArrow");
    }
     

    public void onStartScreen() {
       
    }

    public void onEndScreen() {
      
    }

    public void setArrowWhite(int arrow) {
        NiftyRenderEngine renderEngine = nifty.getRenderEngine();
        Screen currentScreen = nifty.getCurrentScreen();
        
        if (arrow == 1) {
            //Left Arrow
            arrowWhite = renderEngine.createImage(currentScreen, "Interface/Images/whiteArrowLeft.png", false);
            leftArrow.getRenderer(ImageRenderer.class).setImage(arrowWhite);
        } else if (arrow == 2) {
            //Right Arrow
                                                             //nifty.getCurrentScreen()
            arrowWhite = renderEngine.createImage(currentScreen,"Interface/Images/whiteArrowRight.png", false);
            rightArrow.getRenderer(ImageRenderer.class).setImage(arrowWhite);
        } else if (arrow == 3) {
            //Up Arrow
            arrowWhite = renderEngine.createImage(currentScreen, "Interface/Images/whiteArrowUp.png", false);
            upArrow.getRenderer(ImageRenderer.class).setImage(arrowWhite);
        } else if (arrow == 4){
            //Down Arrow
            arrowWhite = renderEngine.createImage(currentScreen, "Interface/Images/whiteArrowDown.png", false);
            downArrow.getRenderer(ImageRenderer.class).setImage(arrowWhite);
        }
    }

    public void setArrowRed(int arrow) {
        NiftyRenderEngine renderEngine = nifty.getRenderEngine();
        Screen currentScreen = nifty.getCurrentScreen();
        
        if (arrow == 1) {
            //Left Arrow
            arrowRed = renderEngine.createImage(currentScreen, "Interface/Images/redArrowLeft.png", false);
            leftArrow.getRenderer(ImageRenderer.class).setImage(arrowRed);
        } else if (arrow == 2) {
            //Right Arrow
            arrowRed = renderEngine.createImage(currentScreen, "Interface/Images/redArrowRight.png", false);
            rightArrow.getRenderer(ImageRenderer.class).setImage(arrowRed);
        } else if (arrow == 3) {
            //Up Arrow
            arrowRed = renderEngine.createImage(currentScreen, "Interface/Images/redArrowUp.png", false);
            upArrow.getRenderer(ImageRenderer.class).setImage(arrowRed);
        } else if (arrow == 4) {
            //Down Arrow
            arrowRed = renderEngine.createImage(currentScreen, "Interface/Images/redArrowDown.png", false);
            downArrow.getRenderer(ImageRenderer.class).setImage(arrowRed);
        }
    }
}
