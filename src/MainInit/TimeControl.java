package MainInit;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;



public class TimeControl extends AbstractControl {
    
    private float time;
    private float timer;
    
    public TimeControl(float time)
    {
        this.time = time;
    }


    @Override
    protected void controlUpdate(float tpf) {
        timer+= tpf;
        if(timer>= time)
        {
            spatial.removeFromParent();
        }
    }

    @Override
    protected void controlRender(RenderManager rm, ViewPort vp) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
