package MainInit;

import Atmosphere.Music;
import Items.Hammer;
import Level.SafeRoom;
import Player.ItemRayCast;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.bullet.BulletAppState;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;

public class Setup {
    
    private static AssetManager assetManager;
    private static BulletAppState bulletAppState;
    private static Node rootNode;
    private static ViewPort viewPort;
    private static Camera cam;
    private static AudioRenderer audioRenderer;
    
    public static void setup(AssetManager assetManager,BulletAppState bulletAppState,Node rootNode,ViewPort viewPort,Camera cam)
    {
        Setup.assetManager = assetManager;
        Setup.bulletAppState = bulletAppState;
        Setup.cam = cam;
        Setup.viewPort = viewPort;
        Setup.rootNode = rootNode;
        
        Particles.setup(assetManager, viewPort);
        Factory.setup(assetManager, bulletAppState);
        Boss.setup(assetManager, bulletAppState);
        SafeRoom.setup(assetManager, bulletAppState);
        Hammer.setup2(bulletAppState, assetManager);
        ItemRayCast.getCam(cam);
        Music backgroundMusic = new Music(rootNode, assetManager);
        Atmosphere.MoneySound.setup(assetManager);
        Atmosphere.ExtraLifeSound.setup(assetManager);
    }
    
    public static void create()
    {
        //Partocles and fog
        rootNode.attachChild(Particles.attach());
        
        //Main Enemy 
        Boss.create(rootNode);
        
        //Spawn Room
        rootNode.attachChild(SafeRoom.createLevel());
        
        //Hammer
        
        //Item check
        
        //Factory
        
    }
}
