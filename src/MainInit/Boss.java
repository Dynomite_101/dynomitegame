package MainInit;

import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.scene.debug.SkeletonDebugger;

public class Boss {
    private static AssetManager assetManager;
    private static BulletAppState bulletAppState;
    public static Node model;
    public static KinematicRagdollControl ragdoll;
    public static AnimControl animControl;
    public static AnimChannel animChannel;
    public static BetterCharacterControl viewDirection = new BetterCharacterControl(4,8,1000){
            @Override
            public void update(float tpf) {
                super.update(tpf);
                setViewDirection(Player.Player.getPlayerLocation().subtract(location));
            }
    };
    
    public static void setup(AssetManager assetManager, BulletAppState bulletAppState) {
        Boss.assetManager = assetManager;
        Boss.bulletAppState = bulletAppState;
    }
    
    public static void create(Node level)
    {
        Node boss = new Node("BossNode");
        boss.setLocalTranslation(79, 9, 79);
        level.attachChild(boss);
        makeEnemy(boss);
    }
    
    private static void makeEnemy(Node boss) {
        model = (Node) assetManager.loadModel("Models/Sinbad/Sinbad.mesh.xml");
        model.setLocalScale(2f);
        //model.setLocalTranslation(79, 9, 79);
        model.setName("Boss");
        boss.attachChild(model);
        
        AnimControl control = model.getControl(AnimControl.class);
        SkeletonDebugger skeletonDebug = new SkeletonDebugger("skeleton", control.getSkeleton());
        Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat2.getAdditionalRenderState().setWireframe(true);
        mat2.setColor("Color", ColorRGBA.Green);
        
        skeletonDebug.setMaterial(mat2);
        //skeletonDebug.setLocalTranslation(model.getLocalTranslation());

        RagdollCollisions listen = new RagdollCollisions();
        
        ragdoll = new KinematicRagdollControl(0.5f);
        setupSinbad(ragdoll);
        ragdoll.addCollisionListener(listen);
       
        getPhysicsSpace().add(ragdoll);
        model.addControl(ragdoll);
        animChannel = control.createChannel();
        
        model.addControl(viewDirection);
    }
    
     private static void setupSinbad(KinematicRagdollControl ragdoll) {
        ragdoll.addBoneName("Ulna.L");
        ragdoll.addBoneName("Ulna.R");
        ragdoll.addBoneName("Chest");
        ragdoll.addBoneName("Foot.L");
        ragdoll.addBoneName("Foot.R");
        ragdoll.addBoneName("Hand.R");
        ragdoll.addBoneName("Hand.L");
        ragdoll.addBoneName("Neck");
        ragdoll.addBoneName("Root");
        ragdoll.addBoneName("Stomach");
        ragdoll.addBoneName("Waist");
        ragdoll.addBoneName("Humerus.L");
        ragdoll.addBoneName("Humerus.R");
        ragdoll.addBoneName("Thigh.L");
        ragdoll.addBoneName("Thigh.R");
        ragdoll.addBoneName("Calf.L");
        ragdoll.addBoneName("Calf.R");
        ragdoll.addBoneName("Clavicle.L");
        ragdoll.addBoneName("Clavicle.R");
    }
     
     private static PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
     }
}
