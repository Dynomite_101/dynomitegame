package MainInit;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import de.lessvoid.nifty.Nifty;
import java.util.Random;

public class qteAppState extends AbstractAppState implements ActionListener {

    private AssetManager assetManager;
    private InputManager inputManager;
    private AudioRenderer audioRenderer;
    private ViewPort guiViewPort;
    private QTEController quickTimeEvent;
    private NiftyJmeDisplay niftyDisplay;
    private Nifty nifty;
    private Random randNum = new Random();
    private int randomInt = 0;
    private boolean qteCheck = false;
    private int qtePress = -1;
    private float qteTimer = 3;
    private int qteControlCount = 0;
    private boolean qteControl = true;
    private AppStateManager stateManager;
    private int hit = 0;
    private Node rootNode;
    private int previousRandInt = 0;

    public qteAppState(AssetManager assetManager, InputManager inputManager,
            AudioRenderer audioRenderer, ViewPort guiViewPort,
            AppStateManager stateManager, Node rootNode) {
        this.assetManager = assetManager;
        this.inputManager = inputManager;
        this.audioRenderer = audioRenderer;
        this.guiViewPort = guiViewPort;
        this.stateManager = stateManager;

        this.rootNode = rootNode;

        inputManager.addMapping("qteUp", new KeyTrigger(KeyInput.KEY_UP));
        inputManager.addMapping("qteDown", new KeyTrigger(KeyInput.KEY_DOWN));
        inputManager.addMapping("qteLeft", new KeyTrigger(KeyInput.KEY_LEFT));
        inputManager.addMapping("qteRight", new KeyTrigger(KeyInput.KEY_RIGHT));
        inputManager.addListener(this, "qteUp", "qteDown", "qteLeft", "qteRight");
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        nifty = ((MainInit.Main) app).getNifty();
        nifty.fromXml("Interface/QuickTimeEvents.xml", "qte", new QTEController());
        quickTimeEvent = (QTEController) nifty.findScreenController("MainInit.QTEController");
    }

    @Override
    public void update(float tpf) {

        if (hit >= 3) {
            if (Main.distanceCheck == false) {
                Boss.model.removeControl(Boss.viewDirection);
                Boss.ragdoll.setRagdollMode();
                Player.Player.addMoney(250);
                Player.Player.keyKill = true;
            }
            qteControlCount = 0;
            stateManager.detach(this);
            nifty.gotoScreen("empty");

            qteTimer = 2;
            hit = 0;
        }

        if (qteControl) {
            quickTimeEvents();
            qteControl = false;
            qteTimer = 2;
        }

        if (qtePress == randomInt || qteTimer < 0) {
            if (qtePress == randomInt) {
                hit++;
            } else {
                loseHealth();
            }
            qteTimer = 2;
            quickTimeEvent.setArrowWhite(randomInt);
            quickTimeEvents();
        }
        qteTimer -= tpf;
        qtePress = -1;
    }

    public void quickTimeEvents() {
        randomInt = randNum.nextInt(4) + 1;
        while (randomInt == previousRandInt) {
            randomInt = randNum.nextInt(4) + 1;
        }
        //System.out.println(randomInt);
        quickTimeEvent.setArrowRed(randomInt);
        qteCheck = true;

        previousRandInt = randomInt;
    }

    public void onAction(String binding, boolean isPressed, float tpf) {
        if (isPressed) {
            if (binding.equals("qteUp")) {
                qtePress = 3;
            } else if (binding.equals("qteDown")) {
                qtePress = 4;
            } else if (binding.equals("qteLeft")) {
                qtePress = 1;
            } else if (binding.equals("qteRight")) {
                qtePress = 2;
            }
            if (qtePress != randomInt) {
                loseHealth();
            }
        }
    }

    public void loseHealth() {
        Player.Player.loseLife(1);
        if (Player.Player.getLives() < 1) {
            rootNode.detachAllChildren();
            stateManager.detach(this);
            nifty.gotoScreen("gameOver");
        }
    }
}
