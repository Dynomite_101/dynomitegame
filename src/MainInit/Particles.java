package MainInit;

import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.effect.shapes.EmitterBoxShape;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.FogFilter;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;

public class Particles {
    
    private static AssetManager assetManager;
    private static ViewPort viewPort;
    private static Node particle;
    public static ParticleEmitter debris;
    
    public static void setup(AssetManager assetManager, ViewPort viewPort) {
        Particles.assetManager = assetManager;
        Particles.viewPort = viewPort;
    }
    
    public static Node attach()
    {
        particle = new Node();
        fogEffect();    
        darknessFog();
        wallParticalSetup();
        return particle;
    }
    
    public static void fogEffect () {
        ParticleEmitter fog = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 100);
        Material mat_white = new Material(assetManager,"Common/MatDefs/Misc/Particle.j3md");
        mat_white.setTexture("Texture", assetManager.loadTexture("Effects/Smoke/Smoke.png"));
        fog.setMaterial(mat_white);
        fog.setImagesX(15);
        fog.setImagesY(1);
        fog.setEndColor(new ColorRGBA(1f, 1f, 1f, .1f));
        fog.setStartColor(new ColorRGBA(1f, 1f, 1f, .1f));
        fog.getParticleInfluencer().setInitialVelocity(new Vector3f(0,1,0));
        fog.setStartSize(10f);
        fog.setEndSize(10f);
        fog.setGravity(0, 0, 0);
        fog.setLowLife(15f);
        fog.setHighLife(20f);
        fog.getParticleInfluencer().setVelocityVariation(0f);
        fog.setShape(new EmitterBoxShape (new Vector3f(0,-4,0), new Vector3f(158,-4,158)));
        particle.attachChild(fog);
    }
    
    public static void darknessFog()
    {
        FilterPostProcessor fpp=new FilterPostProcessor(assetManager);
        FogFilter fog=new FogFilter();
        fog.setFogColor(new ColorRGBA(0.1f, 0.1f, 0.1f, 0.1f));
        fog.setFogDistance(155);
        fog.setFogDensity(6.0f);
        fpp.addFilter(fog);
        viewPort.addProcessor(fpp);
    }
    
    public static void wallParticalSetup()
    {
        debris = new ParticleEmitter("Debris", ParticleMesh.Type.Triangle, 10);
        Material debris_mat = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
        debris_mat.setTexture("Texture", assetManager.loadTexture("Effects/Explosion/Debris.png"));

        debris.setMaterial(debris_mat);
        debris.setImagesX(3); 
        debris.setImagesY(3); // 3x3 texture animation
        debris.setRotateSpeed(10);
        debris.setSelectRandomImage(true);
        debris.getParticleInfluencer().setInitialVelocity(new Vector3f(0, 4, 0));
        debris.setStartColor(ColorRGBA.White);
        debris.setGravity(0, 3, 0);
        debris.setNumParticles(50);
        debris.setParticlesPerSec(50);
        debris.setStartSize(0.4f);
        debris.setEndSize(0.4f);
        debris.getParticleInfluencer().setVelocityVariation(.60f);
    }
    
    public static ParticleEmitter wallBreak()
    {
        debris.emitAllParticles();
        return debris;
    }
}
