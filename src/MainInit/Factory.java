package MainInit;

import com.jme3.asset.AssetManager;
import com.jme3.asset.AssetNotFoundException;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.PlaneCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Plane;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import java.util.List;

//@auther BZammit
public class Factory {

    private static AssetManager assetManager;
    private static BulletAppState bulletAppState;

    private Factory() {
    }

    /**
     * to be called if AssetNotFoundException
     * @return Spatial; containing a small magenta box, wrapped in a Node
     */
    private static Spatial assetLoadFailModel() {
        Node model = new Node("Failed Model");
        Spatial mesh = new Geometry("Failed Mesh", new Box(1, 1, 1));
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");

        mat.setColor("Color", ColorRGBA.Magenta);
        mesh.setMaterial(mat);
        model.attachChild(mesh);
        return model;
    }

    /**
     * sets up Factory
     * @param assetManager
     * @param bulletAppState 
     */
    public static void setup(AssetManager assetManager, BulletAppState bulletAppState) {
        Factory.assetManager = assetManager;
        Factory.bulletAppState = bulletAppState;
    }

    /**
     * loads up floor model
     * <p>
     * if Model doesnt not load properly, returns small magenta box
     * @param length    length of model
     * @param width     width of model
     * @param depth     depth of model
     * @return Spatial; textured floor model, wrapped in a Node
     */
    public static Spatial constructFloor(int length, int width, int depth) {
        Node floor = new Node("FloorNode");
        Spatial floorMesh;
        Material floorText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        try {
            floorMesh = (Node) assetManager.loadModel("Models/floor.j3o");
            List<Spatial> floors = Utilities.findAll("Floor", floorMesh);
            floorMesh = floors.get(0);

            floorMesh.scale((1f / 80f), 0, (1f / 80f));
            floorMesh.scale(length, depth, width);

            floorMesh.setName("Floor");
            floorText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/floortest.png"));
            floorText.setBoolean("VertexLighting", false);

            floorMesh.setMaterial(floorText);
            floor.attachChild(floorMesh);
        } catch (AssetNotFoundException anfe) {
            floor = (Node) Factory.assetLoadFailModel();
        }

        return floor;
    }

    /**
     * creates physical plane to walk on 
     * @param location  is the location of the plane, on the Y-axis
     */
    public static void constructPlane(float location) {
        Plane plane = new Plane(Vector3f.UNIT_Y, location);
        PlaneCollisionShape plane2 = new PlaneCollisionShape(plane);
        RigidBodyControl planePhysics2 = new RigidBodyControl(plane2, 0);
        bulletAppState.getPhysicsSpace().add(planePhysics2);
    }

    /**
     * loads up wall model
     * <p>
     * if model does not load properly, model is replaced by box of allocated size
     * if texture does not load properly, the color if wall will be brown
     * @param length    length of model
     * @param width     width of model
     * @param depth     depth of model
     * @return Spatial; textured partion model, wrapped in a Node
     */
    public static Spatial constructPartition(int length, int width, int depth) {
        Node partition = new Node("PartitionNode");
        Spatial partitionMesh;
        Material partitionText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        try {
            partitionMesh = assetManager.loadModel("Models/Partition.j3o");
            List<Spatial> parts = Utilities.findAll("Partition1", partitionMesh);
            partitionMesh = parts.get(0);
            partitionMesh.scale(0, -5, 0);
            partitionMesh.scale(length, depth, width);
            partitionMesh.setName("Partition");
        } catch (AssetNotFoundException anfe) {
            partitionMesh = new Geometry("Partition", new Box(length, width, depth));
        }

        try {
            partitionText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/partitionTest.png"));
        } catch (AssetNotFoundException anfe) {
            partitionText.setColor("Diffuse", ColorRGBA.Brown);
        }

        partitionText.setBoolean("VertexLighting", false);
        partitionMesh.setMaterial(partitionText);
        partition.attachChild(partitionMesh);
        return partition;
    }

    /**
     * loads up wall model
     * <p>
     * if model does not load properly, model is replaced by box of allocated size
     * if texture does not load properly, the color if wall will be white
     * @param length    length of model
     * @param width     width of model
     * @param depth     depth of model
     * @return Spatial; textured (no cracks) wall model, wrapped in a Node
     */
    public static Spatial constructUnbreakableWall(int length, int width, int depth) {
        Node wall = new Node("UnbreakableWallNode");
        Spatial wallMesh;
        Material wallText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        try {
            wallMesh = assetManager.loadModel("Models/WallFinal.j3o");
            List<Spatial> walls = Utilities.findAll("Wall1", wallMesh);
            wallMesh = walls.get(0);
            wallMesh.scale((1f / 2f), (1f / 5f), (1f / 6f));
            wallMesh.scale(length, depth, width);
            wallMesh.setName("UnbreakableWall");
        } catch (AssetNotFoundException anfe) {
            wallMesh = new Geometry("UnbreakableWall", new Box(length, depth, width));
        }

        try {
            wallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall4of4Final.png"));
        } catch (AssetNotFoundException anfe) {
            wallText.setColor("Diffuse", ColorRGBA.White);
        }

        wallText.setBoolean("VertexLighting", false);
        wallMesh.setMaterial(wallText);
        wall.attachChild(wallMesh);
        return wall;
    }
    
    /**
     * loads up wall model
     * <p>
     * if model does not load properly, model is replaced by box of allocated size
     * if texture does not load properly, the color if wall will be white
     * @param length    length of model
     * @param width     width of model
     * @param depth     depth of model
     * @return Spatial; textured (no cracks) wall model, wrapped in a Node
     */
    public static Spatial constructBreakableWall(int length, int width, int depth) {
        Node wall = new Node("BreakableWallNode");
        Spatial wallMesh;
        Material wallText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        try {
            wallMesh = (Node) assetManager.loadModel("Models/WallFinal.j3o");
            List<Spatial> walls = Utilities.findAll("Wall1", wallMesh);
            wallMesh = walls.get(0);
            wallMesh.scale((1f / 2f), (1f / 5f), (1f / 6f));
            wallMesh.scale(length, depth, width);
            wallMesh.setName("BreakableWall");
        } catch (AssetNotFoundException anfe) {
            wallMesh = new Geometry("BreakableWall", new Box(length, depth, width));
        }

        try {
            wallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall4of4Final.png"));
        } catch (AssetNotFoundException anfe) {
            wallText.setColor("Diffuse", ColorRGBA.White);
        }

        wallText.setBoolean("VertexLighting", false);
        wallMesh.setMaterial(wallText);
        wall.attachChild(wallMesh);
        return wall;
    }

    /**
     * this turns any material passed in, into a cracked wall texture
     * @param wallText a wall material, already set up
     * @return a wall material, but with a randomized cracked texture
     */
    public static Material randomWallTexture(Material wallText) {
        if (Utilities.randomChance(Utilities.CHANCE_ONE_THIRD)) {
            wallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall1of4Final.png"));
        } else if (Utilities.randomChance(Utilities.CHANCE_HALF)) {
            wallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall2of4Final.png"));
        } else {
            wallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall3of4Final.png"));
        }
        return wallText;
    }
    
    /**
     * loads up torch model
     * @return Spatial; textured Torch model
     */
    public static Spatial constructTorch() {
        Spatial torchMesh = assetManager.loadModel("Models/torch.j3o");
        Material torchText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        torchText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/torchTexturetest1.png"));
        torchMesh.setMaterial(torchText);

        return torchMesh;
    }

    /**
     * loads up partition model
     * @param length unused
     * @param width unused
     * @param depth unused
     * @return Spatial; textured Ice Spike model, wrapped in a Node
     */
    public static Spatial constructDecoOne(int length, int width, int depth) {
        Node iceSpike = new Node("IceSpikeNode");
        Spatial iceSpikeMesh = assetManager.loadModel("Models/IceSpike.j3o");
        Material iceSpikeText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        iceSpikeMesh.setName("DecoOne");
        iceSpikeText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/IceSpikeTexturetest.png"));
        iceSpikeMesh.setMaterial(iceSpikeText);
        iceSpike.attachChild(iceSpikeMesh);

        return iceSpike;
    }

    /**
     * loads up Money model
     * @return Spatial; textured money model, wrapped in a Node
     */
    public static Spatial constructMoney() {
        Node money = new Node("MoneyNode");
        Spatial moneyMesh = assetManager.loadModel("Models/Coins.j3o");
        
        List<Spatial> moneyList = Utilities.findAll("coins11", moneyMesh);
        moneyMesh = moneyList.get(0);
        
        Material moneyText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        moneyMesh.scale(1f / 3f);

        moneyMesh.setName("Money");
        moneyText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/CoinsTexture.png"));
        moneyMesh.setMaterial(moneyText);
        money.attachChild(moneyMesh);

        return money;
    }

    /**
     * creates One Up box
     * @return Spatial; cyan One Up box, wrapped in a Node
     */
    public static Spatial constructOneUp() {
        Node oneUp = new Node("OneUpNode");
        Geometry oneUpMesh = new Geometry("One Up", new Box(1, 1, 1));
        Material oneUpText = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");

        oneUpMesh.setName("One Up");
        oneUpText.setColor("Color", ColorRGBA.Cyan);
        oneUpMesh.setMaterial(oneUpText);
        oneUp.attachChild(oneUpMesh);

        return oneUp;
    }

    /**
     * loads up enemy model
     * <p>
     * if Model doesnt not load properly, returns small magenta box
     * @return Spatial; textured enemy model, wrapped in a Node
     */
    public static Spatial constructEnemy() {
        Node enemy = new Node("EnemyNode");
        Spatial enemyMesh;
        Material enemyText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        try {
            enemyMesh = (Node) assetManager.loadModel("Models/seal.j3o");

            enemyMesh.scale((1f / 3f), (1f / 2f), (1f / 3f));
            enemyMesh.rotate(0, -FastMath.HALF_PI, 0);

            enemyMesh.setName("Enemy");
            enemyText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/seal.png"));
            enemyText.setBoolean("VertexLighting", false);

            enemyMesh.setMaterial(enemyText);
            enemy.attachChild(enemyMesh);
        } catch (AssetNotFoundException anfe) {
            enemy = (Node) Factory.assetLoadFailModel();
        }
        return enemy;
    }
    
    /**
     * creates a key in the shape of a box
     * <p>
     * depending on parameter, box changes colours;
     * 1 = red, 2 = blue, 3 = green
     * @param keyNum chose 1, 2, or 3
     * @return Spatial; coloured Key box, wrapped in a Node
     */
    public static Spatial constructKey(int keyNum) {
        Node key = new Node("Key" + keyNum + "Node");
        Geometry keyMesh = new Geometry("Key " + keyNum, new Box(1, 1, 1));
        Material keyText = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");

        //keyMesh.setName("A Key!");
        if (keyNum == 1) keyText.setColor("Color", ColorRGBA.Red);
        else if (keyNum == 2) keyText.setColor("Color", ColorRGBA.Blue);
        else keyText.setColor("Color", ColorRGBA.Green);
        
        keyMesh.setMaterial(keyText);
        key.attachChild(keyMesh);
        return keyMesh;
    }
}
