package MainInit;

import static MainInit.Utilities.layout;
import com.jme3.math.Vector3f;
import com.jme3.scene.SceneGraphVisitor;
import com.jme3.scene.Spatial;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utilities {

    public enum Environment {

        Spike,
        Wall,
        WallStationary,
        RotatedWall,
        RotatedWallStationary,
        Partition,
        PartitionStationary,
        Empty,
        Nothing,
        Money,
        EnemySpawn,
        OneUp, 
        EnemiesCantBeHere,
        EndDoor,
        FindKey
    };

    public enum Position {

        Down,
        Left,
        Up,
        Right,
        None
    };
    public static final int WORLD_SCALE = 6;
    public static final float CHANCE_FIND_KEY = 0.01f;
    public static final float CHANCE_ONEUP = 0.01f;
    public static final float CHANCE_ENEMY = 0.20f;
    public static final float CHANCE_MONEY = 0.05f;
    public static final float CHANCE_ONE_QUARTER = 0.25f;
    public static final float CHANCE_ONE_THIRD = 0.33f;
    public static final float CHANCE_HALF = 0.50f;
    public static final float CHANCE_TWO_THIRDS = 0.66f;
    public static final float CHANCE_THREE_QUARTERS = 0.75f;
    public static final float CHANCE_LAYOUT_NEWPATH = 1f;
    public static Utilities.Environment[][] layout;
    private static final Random random = new Random();

    public static void setup(Utilities.Environment[][] layout) {
        Utilities.layout = layout;
    }

    public static int getLayoutSize() {
        return layout.length;
    }

    public static int randomNumber(int minimum, int maximum) {
        int number = 0;
        while (number < minimum) {
            number = random.nextInt(maximum);
        }
        return number;
    }

    public static boolean randomChance(float chance) {
        return chance > random.nextFloat();
    }

    public static List<Spatial> findAll(final String name, Spatial node) {
        final List<Spatial> candidates = new ArrayList<Spatial>();

        SceneGraphVisitor visitor = new SceneGraphVisitor() {
            @Override
            public void visit(Spatial spatial) {
                String spatialName = spatial.getName();
                if (spatialName != null && spatialName.equals(name)) {
                    candidates.add(spatial);
                }
            }
        };
        node.depthFirstTraversal(visitor);
        return candidates;
    }

    public static Utilities.Position randomDirection(Utilities.Position lastPosition) {
        Utilities.Position direction = Utilities.Position.None;

        if (Utilities.randomChance(Utilities.CHANCE_ONE_QUARTER) && lastPosition != Utilities.Position.Down) {
            direction = Utilities.Position.Up;
        } else if (Utilities.randomChance(Utilities.CHANCE_ONE_THIRD) && lastPosition != Utilities.Position.Left) {
            direction = Utilities.Position.Right;
        } else if (Utilities.randomChance(Utilities.CHANCE_HALF) && lastPosition != Utilities.Position.Up) {
            direction = Utilities.Position.Down;
        } else if (lastPosition != Utilities.Position.Right) {
            direction = Utilities.Position.Left;
        }

        return direction;
    }

    public static List directionCheck(Vector3f location) {
        int positionX = (int) nearestNumber(Math.round(location.z), 12, 6) / WORLD_SCALE,
                positionZ = (int) nearestNumber(Math.round(location.x), 12, 6) / WORLD_SCALE;

        List<Position> availableDirections = new ArrayList<Position>();

        if (pathCheck((positionX - 1), positionZ)) {
            availableDirections.add(Position.Up);
        }
        if (pathCheck((positionX + 1), positionZ)) {
            availableDirections.add(Position.Down);
        }
        if (pathCheck(positionX, (positionZ - 1))) {
            availableDirections.add(Position.Left);
        }
        if (pathCheck(positionX, (positionZ + 1))) {
            availableDirections.add(Position.Right);
        }

        return availableDirections;
    }

    private static boolean pathCheck(int positionZ, int positionX) {
        try {
            if (layout[positionZ][positionX] == Utilities.Environment.Nothing
                    || layout[positionZ][positionX] == Utilities.Environment.Empty
                    || layout[positionZ][positionX] == Utilities.Environment.EnemySpawn
                    || layout[positionZ][positionX] == Utilities.Environment.Money
                    || layout[positionZ][positionX] == Utilities.Environment.OneUp
                    || layout[positionZ][positionX] == Utilities.Environment.FindKey) {
                return true;
            }
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            return false;
        }
        return false;
    }

    public static Utilities.Position oppositeDirection(Utilities.Position lastDirection) {
        if (lastDirection == null) {
            return Utilities.Position.None;
        }
        if (lastDirection.equals(Utilities.Position.Up)) {
            return Utilities.Position.Down;
        }
        if (lastDirection.equals(Utilities.Position.Down)) {
            return Utilities.Position.Up;
        }
        if (lastDirection.equals(Utilities.Position.Left)) {
            return Utilities.Position.Right;
        }
        if (lastDirection.equals(Utilities.Position.Right)) {
            return Utilities.Position.Left;
        }
        return Utilities.Position.None;
    }

    public static int nearestNumber(float number, int multipleOf, int offset) {
        int min = 0, max = 0;
        for (int ctr = offset; ctr <= layout.length * multipleOf; ctr += multipleOf) {
            if (ctr < number) {
                min = ctr;
            } else {
                max = ctr;
                break;
            }
        }
        if (number - min < max - number) {
            return min;
        } else {
            return max;
        }
    }

    public static boolean isDivisable(float number, float by) {
        for (int ctr = 0; ctr < 26; ctr++) {
            if (number / by == ctr) {
                return true;
            }
        }
        return false;
    }
}
