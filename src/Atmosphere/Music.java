package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class Music {

    public Music(Node rootNode, AssetManager assetManager) {
        AudioNode backgroundLoop;
        backgroundLoop = new AudioNode(assetManager, "Sounds/On ice music loop.ogg", false);
        backgroundLoop.setPositional(false);
        backgroundLoop.setLooping(true);
        backgroundLoop.setVolume(0.2f);
        rootNode.attachChild(backgroundLoop);
        backgroundLoop.play();
    }

   


}
