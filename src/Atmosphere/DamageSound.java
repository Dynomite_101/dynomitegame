package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class DamageSound {
    
        public DamageSound(Node wall, AssetManager assetManager) {
        AudioNode damageSound;
        damageSound = new AudioNode(assetManager, "Sounds/DamageSound.ogg", false);
        damageSound.setPositional(false);
        damageSound.setLooping(false);
        damageSound.setVolume(10f);
        wall.attachChild(damageSound);
        damageSound.play();
        }
}
