package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class DeathSound {

    public DeathSound(Node wall, AssetManager assetManager) {
        AudioNode deathSound;
        deathSound = new AudioNode(assetManager, "Sounds/Death Sound.ogg", false);
        deathSound.setPositional(false);
        deathSound.setLooping(false);
        deathSound.setVolume(1.5f);
        wall.attachChild(deathSound);
        deathSound.play();
    }
}
