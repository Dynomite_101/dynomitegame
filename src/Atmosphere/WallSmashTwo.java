package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class WallSmashTwo {
    
        public WallSmashTwo(Node wall, AssetManager assetManager) {
        AudioNode wallBreakingSoundTwo;
        wallBreakingSoundTwo = new AudioNode(assetManager, "Sounds/Smash2.ogg", false);
        wallBreakingSoundTwo.setPositional(false);
        wallBreakingSoundTwo.setLooping(false);
        wallBreakingSoundTwo.setVolume(3f);
        wall.attachChild(wallBreakingSoundTwo);
        wallBreakingSoundTwo.play();
        }
}
