package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class ExtraLifeSound {
    
    private static AssetManager assetManager;
    
    public static void setup(AssetManager assetManager){
        Atmosphere.ExtraLifeSound.assetManager = assetManager;
    }

    public static void lifeSoundNoise(Node wall) {
        AudioNode extraLifeSound;
        extraLifeSound = new AudioNode(assetManager, "Sounds/ExtraLife.ogg", false);
        extraLifeSound.setPositional(false);
        extraLifeSound.setLooping(false);
        extraLifeSound.setVolume(1f / 2f);
        wall.attachChild(extraLifeSound);
        extraLifeSound.play();
    }
}
