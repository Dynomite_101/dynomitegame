package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class MoneySound {
    
    private static AssetManager assetManager;
    
    public static void setup(AssetManager assetManager){
        MoneySound.assetManager = assetManager;
    }

    public static void moneySoundNoise(Node wall) {
        AudioNode moneySound;
        moneySound = new AudioNode(assetManager, "Sounds/Money Sound.ogg", false);
        moneySound.setPositional(false);
        moneySound.setLooping(false);
        moneySound.setVolume(1f / 2f);
        wall.attachChild(moneySound);
        moneySound.play();
    }
}
