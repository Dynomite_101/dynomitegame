package Atmosphere;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.scene.Node;

public class Wallbreaking {

    public Wallbreaking(Node wall, AssetManager assetManager) {
        AudioNode wallBreakingSound;
        wallBreakingSound = new AudioNode(assetManager, "Sounds/Smashing1.ogg", false);
        wallBreakingSound.setPositional(false);
        wallBreakingSound.setLooping(false);
        wallBreakingSound.setVolume(1f/4f);
        wall.attachChild(wallBreakingSound);
        wallBreakingSound.play();
    }
}
