package Level;

import Items.ItemControl;
import MainInit.Main;
import MainInit.Utilities;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.GhostControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import jme3tools.optimize.GeometryBatchFactory;

public class SafeRoom {

    private static Node level;
    private static AssetManager astManager;
    private static BulletAppState bullAppState;

    public static void setup(AssetManager assetManager, BulletAppState bulletAppState) {
        astManager = assetManager;
        bullAppState = bulletAppState;
    }

    static class floor {

        floor(Node room) {
            Spatial floorMesh = astManager.loadModel("Models/floor.j3o");
            Material floorText = new Material(astManager, "Common/MatDefs/Light/Lighting.j3md");
     
            floorMesh.move(6, 0, 12);

            floorText.setTexture("DiffuseMap", astManager.loadTexture("Textures/floortest.png"));
            floorMesh.setMaterial(floorText);
            floorMesh.scale(0.15f);

            room.attachChild(floorMesh);
        }
    }

    static class wall {

        wall(float offset, float offset2, float rotate, Node room) {

            Spatial insideWall = astManager.loadModel("Models/WallFinal.j3o");

            Material wallText = new Material(astManager, "Common/MatDefs/Light/Lighting.j3md");
            wallText.setTexture("DiffuseMap", astManager.loadTexture("Textures/Wall4of4Final.png"));

            insideWall.setMaterial(wallText);

            insideWall.move(offset2, 5f, offset);
            insideWall.rotate(0, rotate, 0);

            room.attachChild(insideWall);
            insideWall.addControl(new RigidBodyControl(0));
            bullAppState.getPhysicsSpace().add(insideWall);

        }
    }

    public static Node createLevel() {
        level = new Node();
        Node spawnRoom = new Node("SpawnRoom");
        Node finishRoom = new Node("FinishRoom");
        level.attachChild(spawnRoom);
        level.attachChild(finishRoom);

        spawnRoom.setLocalTranslation(-6, -1, 24);
        spawnRoom.rotate(0, 3.14159f, 0);
        SafeRoom.floor floor = new SafeRoom.floor(spawnRoom);
        genWalls(spawnRoom);
        spawnRoom.attachChild(hitBoxBuy());

        finishRoom.setLocalTranslation((Utilities.getLayoutSize()) * Utilities.WORLD_SCALE, -1,
                (Utilities.getLayoutSize() - 5f) * Utilities.WORLD_SCALE);
        finishRoom.rotate(0, 0, 0);
        SafeRoom.floor finishFloor = new SafeRoom.floor(finishRoom);
        genWalls(finishRoom);
        finishRoom.attachChild(hitBoxNewGame());

        GeometryBatchFactory.optimize(level);
        return level;
    }

    private static void genWalls(Node room) {
        float offset = 0;
        float offset2 = 0;
        float rotate = 0;
        for (int i = 0; i < 7; i++) {
            SafeRoom.wall wall = new SafeRoom.wall(offset, offset2, rotate, room);

            if (i <= 1) {
                offset2 = 12;
            } else if (i == 2) {
                offset2 = 18;
                offset = 6;
                rotate = 1.570795f;
            } else if (i == 3) {
                offset = 18;
            } else if (i == 4) {
                offset2 = 12;
                offset = 24;
                rotate = 0;
            } else {
                offset2 = 0;
                offset = 24;
            }
        }
    }

    private static Spatial hitBoxBuy() {
        final Node buyBox = new Node("BuyBox");
        
        buyBox.move(6, 1, 12);
        
        buyBox.addControl(new GhostControl(new BoxCollisionShape(new Vector3f(11, 1, 11))));
        bullAppState.getPhysicsSpace().add(buyBox);
        bullAppState.getPhysicsSpace().addCollisionListener(new PhysicsCollisionListener() {

            public void collision(PhysicsCollisionEvent event) {
                Spatial node;
                if (event.getNodeA() == (buyBox)) {
                    node = event.getNodeB();
                } else if (event.getNodeB() == (buyBox)) {
                    node = event.getNodeA();
                } else {
                    return;
                }

                if (node == MainInit.Main.player) {
                    Main.ableToBuy = true;
                } else {
                    Main.ableToBuy = false;
                }
            }
        });
        return buyBox;
    }
    
    private static Spatial hitBoxNewGame() {
        final Node newGameBox = new Node("NewGameBox");
        
        newGameBox.move(6, 1, 12);
        
        newGameBox.addControl(new GhostControl(new BoxCollisionShape(new Vector3f(11, 1, 11))));
        bullAppState.getPhysicsSpace().add(newGameBox);
        bullAppState.getPhysicsSpace().addCollisionListener(new PhysicsCollisionListener() {

            public void collision(PhysicsCollisionEvent event) {
                Spatial node;
                if (event.getNodeA() == (newGameBox)) {
                    node = event.getNodeB();
                } else if (event.getNodeB() == (newGameBox)) {
                    node = event.getNodeA();
                } else {
                    return;
                }

                if (node == MainInit.Main.player) {
                    Main.newGame = true;
                }
            }
        });
        return newGameBox;
    }
}
