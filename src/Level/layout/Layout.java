package Level.layout;

public interface Layout {

    void newLayout();

    void createLayout();

    MainInit.Utilities.Environment[][] getLayout();
}
