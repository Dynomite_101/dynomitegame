package Level.layout;

import MainInit.Utilities;

public abstract class DefaultLayout implements Level.layout.Layout {

    protected Utilities.Environment[][] layout;
    protected int positionYStart = 1, positionXStart = 1;
    private final int DONE = 0, CHANGE = 1, NO_CHANGE = 2;

    public DefaultLayout() {
    }

    public void createLayout() {
        if (layout == null) {
            throw new NullPointerException("Layout equals Null");
        }

        while (!done()) {
            buildPath(positionYStart, positionXStart, Utilities.Position.None);
        }
    }

    private boolean done() {
        for (int positionY = 1; positionY < layout.length; positionY += 2) {
            for (int positionX = 1; positionX < layout[positionY].length; positionX += 2) {
                if (layout[positionY][positionX] == Utilities.Environment.Empty) {
                    positionYStart = positionY;
                    positionXStart = positionX;
                    return false;
                }
            }
        }
        return true;
    }

    private void buildPath(int positionY, int positionX, Utilities.Position lastPosition) {
        int condition = NO_CHANGE, newPathCounter = 0;

        if (positionY < 0 || positionY >= layout.length) {
            return;
        }
        if (positionX < 0 || positionX >= layout[positionY].length) {
            return;
        }

        while (true) {
            if (Utilities.randomChance(Utilities.CHANCE_MONEY)) {
                layout[positionY][positionX] = Utilities.Environment.Money;
            } else if (Utilities.randomChance(Utilities.CHANCE_ONEUP)) {
                layout[positionY][positionX] = Utilities.Environment.OneUp;
            } else if (Utilities.randomChance(Utilities.CHANCE_ENEMY)) {
                layout[positionY][positionX] = Utilities.Environment.EnemySpawn;
            } else if (Utilities.randomChance(Utilities.CHANCE_FIND_KEY)) {
                layout[positionY][positionX] = Utilities.Environment.FindKey;
            } else {
                layout[positionY][positionX] = Utilities.Environment.Nothing;
            }

            Utilities.Position direction = Utilities.randomDirection(lastPosition);

            if (direction == Utilities.Position.Up) {
                condition = moveInDirection(positionY, positionX, lastPosition, Utilities.Position.Up);
                if (condition == CHANGE) {
                    lastPosition = Utilities.Position.Up;
                    positionY -= 2;
                }
            } else if (direction == Utilities.Position.Right) {
                condition = moveInDirection(positionY, positionX, lastPosition, Utilities.Position.Right);
                if (condition == CHANGE) {
                    lastPosition = Utilities.Position.Right;
                    positionX += 2;
                }
            } else if (direction == Utilities.Position.Down) {
                condition = moveInDirection(positionY, positionX, lastPosition, Utilities.Position.Down);
                if (condition == CHANGE) {
                    lastPosition = Utilities.Position.Down;
                    positionY += 2;
                }
            } else {
                condition = moveInDirection(positionY, positionX, lastPosition, Utilities.Position.Left);
                if (condition == CHANGE) {
                    lastPosition = Utilities.Position.Left;
                    positionX -= 2;
                }
            }

            if (condition == DONE || done()) {
                break;
            }

            if (newPathCounter >= 20) {
                done();
                positionY = positionYStart;
                positionX = positionXStart;
                newPathCounter = 0;
            }

            if (condition == NO_CHANGE) {
                newPathCounter++;
            }

            condition = NO_CHANGE;
        }
    }

    private int moveInDirection(int positionY, int positionX, Utilities.Position lastPosition, Utilities.Position currentPosition) {
        int indexY = positionY, indexX = positionX;
        Utilities.Position checkOne, checkTwo;
        boolean upOrDown;

        if (currentPosition == Utilities.Position.Down) {
            indexY++;
            checkOne = Utilities.Position.Left;
            checkTwo = Utilities.Position.Right;
            upOrDown = true;
        } else if (currentPosition == Utilities.Position.Left) {
            indexX--;
            checkOne = Utilities.Position.Down;
            checkTwo = Utilities.Position.Up;
            upOrDown = false;
        } else if (currentPosition == Utilities.Position.Up) {
            indexY--;
            checkOne = Utilities.Position.Left;
            checkTwo = Utilities.Position.Right;
            upOrDown = true;
        } else {
            indexX++;
            checkOne = Utilities.Position.Up;
            checkTwo = Utilities.Position.Down;
            upOrDown = false;
        }

        try {
            if (layout[indexY][indexX].equals(Utilities.Environment.Empty)) {
                layout[indexY][indexX] = Utilities.Environment.Nothing;

                pathCheck(positionY, positionX, lastPosition, currentPosition, checkOne, checkTwo, upOrDown);
            } else if (layout[indexY][indexX].equals(Utilities.Environment.Wall) || layout[indexY][indexX].equals(Utilities.Environment.RotatedWall)) {
                endPath(positionY, positionX, lastPosition);
                return DONE;
            } else {
                return NO_CHANGE;
            }
            return CHANGE;
        } catch (ArrayIndexOutOfBoundsException exception) {
            return DONE;
        }
    }

    private void pathCheck(int positionY, int positionX, Utilities.Position lastPosition, Utilities.Position currentPosition, Utilities.Position checkOne, Utilities.Position checkTwo, boolean upOrDown) {
        if (Utilities.randomChance(Utilities.CHANCE_LAYOUT_NEWPATH)) {
            if (upOrDown) {
                positionCheck(positionY, positionX - 1, lastPosition, currentPosition, checkOne, Utilities.Environment.Wall);
                positionCheck(positionY, positionX + 1, lastPosition, currentPosition, checkTwo, Utilities.Environment.Wall);
            } else {
                positionCheck(positionY - 1, positionX, lastPosition, currentPosition, checkOne, Utilities.Environment.RotatedWall);
                positionCheck(positionY + 1, positionX, lastPosition, currentPosition, checkTwo, Utilities.Environment.RotatedWall);
            }
        } else {
            if (upOrDown) {
                newPath(positionY, positionX - 2);
                newPath(positionY, positionX + 2);
            } else {
                newPath(positionY - 2, positionX);
                newPath(positionY + 2, positionX);
            }
        }
    }

    private void positionCheck(int positionY, int positionX, Utilities.Position lastPosition, Utilities.Position currentPosition, Utilities.Position check, Utilities.Environment changeTo) {
        if (lastPosition.equals(currentPosition) || lastPosition.equals(check)) {
            if (layout[positionY][positionX].equals(Utilities.Environment.Empty)) {
                layout[positionY][positionX] = changeTo;
            }
        }
    }

    private void newPath(int positionY, int positionX) {
        try {
            if (layout[positionY][positionX].equals(Utilities.Environment.Empty)) {
                buildPath(positionY, positionX, Utilities.Position.None);
            }
        } catch (ArrayIndexOutOfBoundsException exception) {
        }
    }

    private void endPath(int positionY, int positionX, Utilities.Position lastPosition) {
        if (lastPosition.equals(Utilities.Position.Left) || lastPosition.equals(Utilities.Position.Right)) {
            if (layout[positionY + 1][positionX].equals(Utilities.Environment.Empty)) {
                layout[positionY + 1][positionX] = Utilities.Environment.RotatedWall;
            }
            if (layout[positionY - 1][positionX].equals(Utilities.Environment.Empty)) {
                layout[positionY - 1][positionX] = Utilities.Environment.RotatedWall;
            }
        } else {
            if (layout[positionY][positionX + 1].equals(Utilities.Environment.Empty)) {
                layout[positionY][positionX + 1] = Utilities.Environment.Wall;
            }
            if (layout[positionY][positionX - 1].equals(Utilities.Environment.Empty)) {
                layout[positionY][positionX - 1] = Utilities.Environment.Wall;
            }
        }
    }

    public Utilities.Environment[][] getLayout() {
        return layout;
    }
}