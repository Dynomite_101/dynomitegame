package Level;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.joints.HingeJoint;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;

public class Sign {
    
    Box sign = new Box(1f, 0.5f, 0.2f);    
    Box signHolder = new Box(2f, 0.2f, 0.2f);
    Geometry signGeom = new Geometry("Box", sign);
    Geometry signHolderGeom = new Geometry("Box", signHolder);
    private HingeJoint joint;
    
    public Sign(Node rootNode, AssetManager assetManager, BulletAppState bulletAppState){
        
        Material signMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        //signMat.setTexture("ColorMap", assetManager.loadTexture("Textures/torchTexturetest1.png"));
        signMat.setColor("Color", ColorRGBA.Yellow);
        signGeom.setMaterial(signMat);
        rootNode.attachChild(signGeom);
        RigidBodyControl signRig = new RigidBodyControl(1);
        signGeom.addControl(signRig);
        
        
        Material signHolderMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        signHolderMat.setColor("Color", ColorRGBA.White);
        signHolderGeom.setMaterial(signHolderMat);
        rootNode.attachChild(signHolderGeom);
        
        
        RigidBodyControl signHolderRig = new RigidBodyControl(0);
        signHolderGeom.addControl(signHolderRig);

        joint = new HingeJoint(signRig, signHolderRig,
                new Vector3f(0f, .6f, 0f), // pivot point local to A
                new Vector3f(0f, -.6f, 0f), // pivot point local to B 
                Vector3f.UNIT_X, // DoF Axis of A (Z axis)
                Vector3f.UNIT_X);

        bulletAppState.getPhysicsSpace().add(joint);
        bulletAppState.getPhysicsSpace().add(signHolderRig);
        bulletAppState.getPhysicsSpace().add(signRig);
        signHolderRig.setPhysicsLocation(new Vector3f(3f,7f,12f));
        signRig.setPhysicsLocation(new Vector3f(3f,5f,12f));
        signRig.applyImpulse(Vector3f.UNIT_Z.mult(-.5f), Vector3f.ZERO);
        
    }
            
}
