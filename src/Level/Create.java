package Level;

import Items.Hammer;
import Items.TorchPlacing;
import MainInit.Utilities;
import Player.ItemRayCast;
import com.jme3.bullet.BulletAppState;
import com.jme3.scene.Node;
import jme3tools.optimize.GeometryBatchFactory;

public class Create {
    
    static Node level;
    public static int numberOfKey3 = 0;

    public Node createLevel(Level.Staging staging, Utilities.Environment[][] layout, BulletAppState bulletAppState) {
        Node items = new Node("Items");
        Node breakable = new Node("Breakable");
        Node unbreakable = new Node("Unbreakable");
        level = new Node("Level");
        level.attachChild(items);
        level.attachChild(breakable);
        level.attachChild(unbreakable);
        
        
        
        ItemRayCast.getNode(items);
        unbreakable.attachChild(staging.floor(78, 78));

        for (int positionX = 0; positionX < layout.length; positionX++) {
            for (int positionZ = 0; positionZ < layout[positionX].length; positionZ++) {
                if (layout[positionZ][positionX] == Utilities.Environment.Nothing) {
                } else if (layout[positionZ][positionX] == Utilities.Environment.PartitionStationary) {
                    unbreakable.attachChild(staging.partition(positionX, positionZ));
                } else if (layout[positionZ][positionX] == Utilities.Environment.Partition) {
                    if (partitionCheck(positionX, positionZ, layout)) {
                        unbreakable.attachChild(staging.partition(positionX, positionZ));
                    }
                } else if (layout[positionZ][positionX] == Utilities.Environment.Wall) {
                    breakable.attachChild(staging.wall(positionX, positionZ, false, true));
                } else if (layout[positionZ][positionX] == Utilities.Environment.WallStationary) {
                    unbreakable.attachChild(staging.wall(positionX, positionZ, false, false));
                } else if (layout[positionZ][positionX] == Utilities.Environment.RotatedWall) {
                    breakable.attachChild(staging.wall(positionX, positionZ, true, true));
                } else if (layout[positionZ][positionX] == Utilities.Environment.RotatedWallStationary) {
                    unbreakable.attachChild(staging.wall(positionX, positionZ, true, false));
                } else if (layout[positionZ][positionX] == Utilities.Environment.Spike) {
                    unbreakable.attachChild(staging.decorationOne(positionX, positionZ));
                    unbreakable.attachChild(staging.partition(positionX, positionZ));
                } else if (layout[positionZ][positionX] == Utilities.Environment.Money) {
                    items.attachChild(staging.money(positionX, positionZ));
                } else if (layout[positionZ][positionX] == Utilities.Environment.OneUp) {
                    items.attachChild(staging.oneUp(positionX, positionZ));
                } else if (layout[positionZ][positionX] == Utilities.Environment.EndDoor) {
                    level.attachChild(staging.endDoor(positionX, positionZ));
                } else if (layout[positionZ][positionX] == Utilities.Environment.FindKey && numberOfKey3 < 1 && Player.Player.keyFind == false) {
                    items.attachChild(staging.findKey(positionX, positionZ));
                    numberOfKey3++;
                }
            }
        }
        
        if (numberOfKey3 < 1) {
            items.attachChild(staging.findKey(1,25));
            numberOfKey3++;
        }

        GeometryBatchFactory.optimize(unbreakable);
        Hammer.setup(breakable);
        TorchPlacing.setup(level);
        
        return level;
    }

    private boolean partitionCheck(int positionX, int positionZ, Utilities.Environment[][] layout) {
        if (layout[positionZ + 1][positionX].equals(Utilities.Environment.Empty) || layout[positionZ + 1][positionX].equals(Utilities.Environment.Nothing)) {
            if (layout[positionZ][positionX + 1].equals(Utilities.Environment.Empty) || layout[positionZ][positionX + 1].equals(Utilities.Environment.Nothing)) {
                if (layout[positionZ - 1][positionX].equals(Utilities.Environment.Empty) || layout[positionZ - 1][positionX].equals(Utilities.Environment.Nothing)) {
                    if (layout[positionZ][positionX - 1].equals(Utilities.Environment.Empty) || layout[positionZ][positionX - 1].equals(Utilities.Environment.Nothing)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
    
    public static Node getLevel() {
        return level;
    }
}
