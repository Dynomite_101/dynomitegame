package Level;

import Items.ItemControl;
import MainInit.Factory;
import MainInit.Utilities;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.LightNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import java.util.List;

public class IceStaging implements Staging {

    private final Vector3f FLOOR_LOCATION = new Vector3f(0, -1f, 0);
    private final Vector3f WALL_LOCATION = new Vector3f(0, 4, 0);
    private static final Vector3f TORCH_LOCATION_RELATIVE_TO_WALL = new Vector3f(0, 2, 1.25f);
    private static final Vector3f TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH = new Vector3f(.5f, 1.25f, .5f);
    private final Vector3f ICESPIKE_LOCATION = new Vector3f(0, 9, 0);
    private final Vector3f ITEM_LOCATION = new Vector3f(0, 1.5f, 0);
    private final Vector3f KEY1_LOCATION_RELATIVE_TO_WALL = new Vector3f(-.2f, 0, -2);
    private final Vector3f KEY2_LOCATION_RELATIVE_TO_WALL = new Vector3f(-.2f, 0, 0);
    private final Vector3f KEY3_LOCATION_RELATIVE_TO_WALL = new Vector3f(-.2f, 0, 2);
    private static final float RAD_ROTATE_15_DEG = FastMath.DEG_TO_RAD * 15;
    private static final float RAD_ROTATE_90_DEG = FastMath.DEG_TO_RAD * 90;
    private static final float RAD_ROTATE_180_DEG = FastMath.DEG_TO_RAD * 180;
    private static final float RAD_ROTATE_345_DEG = FastMath.DEG_TO_RAD * 345;
    private BulletAppState bulletAppState;
    private static Node worldNode;
    private static int counter = 0;

    public void setup(Node worldNode, BulletAppState bulletAppState) {
        IceStaging.worldNode = worldNode;
        this.bulletAppState = bulletAppState;
    }

    public Spatial floor(float positionX, float positionZ) {
        Node floor = new Node("FloorNode");
        floor.attachChild(Factory.constructFloor(80, 80, 1));
        Factory.constructPlane(FLOOR_LOCATION.getY());

        floor.setLocalTranslation(FLOOR_LOCATION.add(positionX, 0, positionZ));

        return floor;
    }

    public Spatial partition(float positionX, float positionZ) {
        Spatial partition = Factory.constructPartition(1, 5, 1);

        partition.setLocalTranslation(WALL_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0, positionZ * Utilities.WORLD_SCALE));

        partition.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(partition);

        return partition;
    }

    public Spatial wall(float positionX, float positionZ, boolean rotate, boolean breakable) {
        Spatial wall;

        if (breakable) {
            wall = Factory.constructBreakableWall(2, 6, 5);
        } else {
            wall = Factory.constructUnbreakableWall(2, 6, 5);
        }

        if (!rotate) {
            wall.rotate(0, RAD_ROTATE_90_DEG, 0);
        }
        wall.setLocalTranslation(WALL_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0, positionZ * Utilities.WORLD_SCALE));

        if (Utilities.randomChance(Utilities.CHANCE_HALF)) {
            wall.rotate(0, RAD_ROTATE_180_DEG, 0);
        }

        wall.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(wall);

        return wall;
    }

    public static void Torch(Node wall) {
        Node torchNode = new Node("Torch");
        Spatial torch = Factory.constructTorch();
        Spatial torch2 = Factory.constructTorch();
        torchNode.attachChild(torch);
        torchNode.attachChild(torch2);
        wall.attachChild(torchNode);

        torch.setLocalTranslation(TORCH_LOCATION_RELATIVE_TO_WALL);
        torch.rotate(RAD_ROTATE_15_DEG, 0, 0);
        torchLight(true, torch.getWorldTranslation().add(TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH), wall);

        torch2.setLocalTranslation(new Vector3f(TORCH_LOCATION_RELATIVE_TO_WALL.x, TORCH_LOCATION_RELATIVE_TO_WALL.y, -TORCH_LOCATION_RELATIVE_TO_WALL.z));
        torch2.rotate(RAD_ROTATE_345_DEG, 0, 0);
        torchLight(false, torch2.getWorldTranslation().add(TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH), wall);
    }

    private static void torchLight(boolean side, Vector3f location, Node wall) {
        if (counter >= 10) {
            return;
        }

        Quaternion thing = wall.getLocalRotation();

        if (wall.getLocalRotation().getW() < 1) {
            if (side) {
                location = location.addLocal(-TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.x, TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.y, 0);
            } else {
                location = location.addLocal(TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.x, TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.y, 0);
            }
        } else {
            if (side) {
                location = location.addLocal(0, TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.y, TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.z);
            } else {
                location = location.addLocal(0, TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.y, -TORCH_LIGHT_LOCATION_RELATIVE_TO_TORCH.z);
            }
        }

        PointLight light = new PointLight();
        LightNode lightNode = new LightNode("pointLight", light);
        lightNode.setName("pointLight");

        light.setColor(ColorRGBA.White.mult(1f));
        light.setRadius(20);
        light.setPosition(location);
        lightNode.setLocalTranslation(location);

        worldNode.addLight(light);
        Spatial test1 = Factory.constructOneUp();
        //worldNode.attachChild(test1);
        worldNode.attachChild(lightNode);

        counter++;
    }

    public Spatial decorationOne(float positionX, float positionZ) {
        Spatial iceSpike = Factory.constructDecoOne(1, 1, 30);

        iceSpike.setLocalTranslation(ICESPIKE_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0, positionZ * Utilities.WORLD_SCALE));

        iceSpike.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(iceSpike);

        return iceSpike;
    }

    public Spatial money(float positionX, float positionZ) {
        final Node money2 = new Node("Money");
        Spatial money = Factory.constructMoney();

        money2.attachChild(money);

        money2.setLocalTranslation(ITEM_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0, positionZ * Utilities.WORLD_SCALE));

        money2.addControl(new ItemControl(new BoxCollisionShape(new Vector3f(1, 1, 1)), money2));
        bulletAppState.getPhysicsSpace().add(money2);
        bulletAppState.getPhysicsSpace().addCollisionListener(new PhysicsCollisionListener() {
            boolean done = false, enemy = false;

            public void collision(PhysicsCollisionEvent event) {
                if (done) {
                    return;
                }

                Spatial node;
                if (event.getNodeA() == (money2)) {
                    node = event.getNodeB();
                } else if (event.getNodeB() == (money2)) {
                    node = event.getNodeA();
                } else {
                    return;
                }

                for (Spatial enemy : Utilities.findAll("EnemyNode", worldNode)) {
                    if (node == enemy) {
                        this.enemy = true;
                        break;
                    }
                }
                if (enemy) {
                    enemy = false;
                } else {
                    Player.Player.addMoney(Utilities.randomNumber(20, 65));
                    Atmosphere.MoneySound.moneySoundNoise(money2);
                    bulletAppState.getPhysicsSpace().remove(money2);
                    money2.removeFromParent();
                    done = true;
                }
            }
        });
        return money2;
    }

    public Spatial oneUp(float positionX, float positionZ) {
        final Node oneUp2 = new Node("OneUp");
        Spatial oneUp = Factory.constructOneUp();

        oneUp2.attachChild(oneUp);

        oneUp2.setLocalTranslation(ITEM_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0, positionZ * Utilities.WORLD_SCALE));

        oneUp2.addControl(new ItemControl(new BoxCollisionShape(new Vector3f(1, 1, 1)), oneUp2));
        bulletAppState.getPhysicsSpace().add(oneUp2);
        bulletAppState.getPhysicsSpace().addCollisionListener(new PhysicsCollisionListener() {
            boolean done = false, enemy = false;

            public void collision(PhysicsCollisionEvent event) {
                while (done) {
                    return;
                }

                Spatial node;
                if (event.getNodeA() == (oneUp2)) {
                    node = event.getNodeB();
                } else if (event.getNodeB() == (oneUp2)) {
                    node = event.getNodeA();
                } else {
                    return;
                }

                for (Spatial enemy : Utilities.findAll("EnemyNode", worldNode)) {
                    if (node == enemy) {
                        this.enemy = true;
                        break;
                    }
                }
                if (enemy) {
                    enemy = false;
                } else {
                    Player.Player.addLife(1);
                    Atmosphere.ExtraLifeSound.lifeSoundNoise(oneUp2);
                    bulletAppState.getPhysicsSpace().remove(oneUp2);
                    oneUp2.removeFromParent();
                    done = true;
                }
            }
        });
        return oneUp2;
    }

    public Spatial endDoor(float positionX, float positionZ) {
        Node endDoor = new Node("EndDoor");
        Node key1 = new Node("Key1");
        Node key2 = new Node("Key2");
        Node key3 = new Node("Key3");
        endDoor.attachChild(key1);
        endDoor.attachChild(key2);
        endDoor.attachChild(key3);

        Spatial endDoorMesh = Factory.constructUnbreakableWall(2, 6, 5);
        endDoorMesh.setName("EndDoorNode");
        Spatial key1Mesh = Factory.constructKey(1);
        Spatial key2Mesh = Factory.constructKey(2);
        Spatial key3Mesh = Factory.constructKey(3);

        key1.attachChild(key1Mesh);
        key2.attachChild(key2Mesh);
        key3.attachChild(key3Mesh);

        endDoor.setLocalTranslation(WALL_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0,
                positionZ * Utilities.WORLD_SCALE));
        key1.setLocalTranslation(KEY1_LOCATION_RELATIVE_TO_WALL);
        key2.setLocalTranslation(KEY2_LOCATION_RELATIVE_TO_WALL);
        key3.setLocalTranslation(KEY3_LOCATION_RELATIVE_TO_WALL);
        endDoorMesh.rotate(0, FastMath.DEG_TO_RAD * 90, 0);

        key1.addControl(new AbstractControl() {
            @Override
            protected void controlUpdate(float tpf) {
                if (Player.Player.keyBuy) {
                    spatial.removeFromParent();
                }
            }

            @Override
            protected void controlRender(RenderManager rm, ViewPort vp) {
            }
        });

        key2.addControl(new AbstractControl() {
            @Override
            protected void controlUpdate(float tpf) {
                if (Player.Player.keyKill) {
                    spatial.removeFromParent();
                }
            }

            @Override
            protected void controlRender(RenderManager rm, ViewPort vp) {
            }
        });

        key3.addControl(new AbstractControl() {
            @Override
            protected void controlUpdate(float tpf) {
                if (Player.Player.keyFind) {
                    spatial.removeFromParent();
                }
            }

            @Override
            protected void controlRender(RenderManager rm, ViewPort vp) {
            }
        });

        endDoor.attachChild(endDoorMesh);
        endDoorMesh.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(endDoorMesh);

        endDoor.addControl(new AbstractControl() {
            @Override
            protected void controlUpdate(float tpf) {
                if (Player.Player.keyBuy
                        && Player.Player.keyKill
                        && Player.Player.keyFind) {
                    List<Spatial> endDoors = Utilities.findAll("EndDoorNode", spatial);
                    for (Spatial endDoor : endDoors) {
                        bulletAppState.getPhysicsSpace().remove(endDoor);
                        endDoor.removeControl(RigidBodyControl.class);
                        endDoor.removeFromParent();

                        try {
                            bulletAppState.getPhysicsSpace().remove(endDoor);
                        } catch (NullPointerException npe) {
                        }

                    }
                    getSpatial().removeFromParent();
                }
            }

            @Override
            protected void controlRender(RenderManager rm, ViewPort vp) {
            }
        });

        return endDoor;
    }

    public Spatial findKey(float positionX, float positionZ) {
        final Node key3 = new Node("Key, you must get!");
        Spatial keyMesh = Factory.constructKey(3);
        keyMesh.setName("A Key!");
        key3.attachChild(keyMesh);

        key3.setLocalTranslation(ITEM_LOCATION.add(positionX * Utilities.WORLD_SCALE, 0, positionZ * Utilities.WORLD_SCALE));

        key3.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(key3);

        key3.addControl(new ItemControl(new BoxCollisionShape(new Vector3f(1, 1, 1)), key3));
        bulletAppState.getPhysicsSpace().add(key3);
        bulletAppState.getPhysicsSpace().addCollisionListener(new PhysicsCollisionListener() {
            boolean done = false;

            public void collision(PhysicsCollisionEvent event) {
                while (done) {
                    return;
                }

                Spatial node;
                if (event.getNodeA() == (key3)) {
                    node = event.getNodeB();
                } else if (event.getNodeB() == (key3)) {
                    node = event.getNodeA();
                } else {
                    return;
                }

                if (node == MainInit.Main.player) {
                    Player.Player.keyFind = true;
                    bulletAppState.getPhysicsSpace().remove(key3);
                    key3.removeFromParent();
                    done = true;
                }
            }
        });
        return key3;
    }
}
