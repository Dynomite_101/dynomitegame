package Level;

import Items.Hammer;
import MainInit.RagdollCollisions;
import MainInit.Utilities;
import com.jme3.animation.AnimControl;
import com.jme3.animation.Bone;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionObject;
import com.jme3.bullet.collision.RagdollCollisionListener;
import com.jme3.bullet.control.KinematicRagdollControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh;
import com.jme3.effect.shapes.EmitterBoxShape;
import com.jme3.light.AmbientLight;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.LightNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.debug.SkeletonDebugger;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Quad;
import jme3tools.optimize.GeometryBatchFactory;

public class LevelCreate implements RagdollCollisionListener{

    private static final Vector3f FLOOR_LOCATION = new Vector3f(78f, -2, 78);
    private static final Vector3f WALL_LOCATION = new Vector3f(0, 4, 0);
    private static final Vector3f ICESPIKE_LOCATION = new Vector3f(0, 9, 0);
    private static final Vector3f TORCH_LOCATION_RELATIVE_TO_WALL = new Vector3f(0, 2, 1.25f);
    private static AssetManager assetManager;
    private static BulletAppState bulletAppState;
    private static ViewPort viewPort;
    private static Node level;
    private static int counter = 0;
    public static KinematicRagdollControl ragdoll, ragdoll2;
    public static Node model, model2;

    private LevelCreate() {
    }

    public static void setup(AssetManager assetManager, BulletAppState bulletAppState, ViewPort viewPort) {
        LevelCreate.assetManager = assetManager;
        LevelCreate.bulletAppState = bulletAppState;
        LevelCreate.viewPort = viewPort;
    }

    public static Node createLevel() {
        if (assetManager == null || bulletAppState == null) {
            return null;
        }

        //Node breakable = new Node();
       // Node unbreakable = new Node();
        level = new Node();
        
        //levelAmbiance(level);
        //unbreakable.attachChild(constructFloor());
        //generatedWalls(unbreakable, true);
        //generatedWalls(breakable, false);
       
        //level.attachChild(unbreakable);
       // GeometryBatchFactory.optimize(level);
        //fogEffect();
        makeEnemy();
        //level.attachChild(breakable);

        //Hammer.setup(breakable);
        return level;
    }

    private static void levelAmbiance(Node level) {
        viewPort.setBackgroundColor(ColorRGBA.Blue.mult(.03f));

        AmbientLight color = new AmbientLight();
        color.setColor(ColorRGBA.Cyan.mult(.1f));
        level.addLight(color);
    }

    public static void fogEffect () {
        ParticleEmitter fog = new ParticleEmitter("Emitter", ParticleMesh.Type.Triangle, 100);
        Material mat_white = new Material(assetManager,"Common/MatDefs/Misc/Particle.j3md");
        mat_white.setTexture("Texture", assetManager.loadTexture("Effects/Smoke/Smoke.png"));
        fog.setMaterial(mat_white);
        fog.setImagesX(15);
        fog.setImagesY(1);
        fog.setEndColor(new ColorRGBA(1f, 1f, 1f, .1f));
        fog.setStartColor(new ColorRGBA(1f, 1f, 1f, .1f));
        fog.getParticleInfluencer().setInitialVelocity(new Vector3f(0,1,0));
        fog.setStartSize(10f);
        fog.setEndSize(10f);
        fog.setGravity(0, 0, 0);
        fog.setLowLife(15f);
        fog.setHighLife(20f);
        fog.getParticleInfluencer().setVelocityVariation(0f);
        fog.setShape(new EmitterBoxShape (new Vector3f(0,-4,0), new Vector3f(158,-4,158)));
        level.attachChild(fog);
    }
    
    private static Node constructFloor() {
        Node floor = new Node();

        createTileFloor(floor);

        Spatial floorMesh = assetManager.loadModel("Models/floor.j3o");
        Material floorText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        floorText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/floortest.png"));
        floorMesh.setMaterial(floorText);
        floor.attachChild(floorMesh);

        floor.setLocalTranslation(FLOOR_LOCATION);

        return floor;
    }

    private static void createTileFloor(Node floor) {
        Node tiles = new Node();
        float offset = (float) 12.1;
        for (int index = 0; index < offset; index++) {
            for (int counter = 0; counter < offset; counter++) {
                Quad b = new Quad(offset, offset);
                Geometry tile = new Geometry("Box", b);

                tile.rotate(-90 * FastMath.DEG_TO_RAD, 0, 0);
                tile.move(offset * index, -1, offset * counter + offset);

                Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                mat.setColor("Color", ColorRGBA.BlackNoAlpha);
                mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
                tile.setMaterial(mat);

                tile.addControl(new RigidBodyControl(0));
                bulletAppState.getPhysicsSpace().add(tile);

                tiles.attachChild(tile);
            }
        }
        floor.attachChild(tiles);
    }

    private static void generatedWalls(Node place, boolean betweeners) {
        for (int vertical = 0; vertical < 2; vertical++) {
            int locationOffset = 6;
            int sets = 13;
            if (betweeners) {
                locationOffset = 0;
                sets = 14;
            }
            for (int set = 0; set < sets; set++) {
                int startOffset = 0;
                for (int horizontal = 0; horizontal < 14; horizontal++) {
                    if (betweeners) {
                        constructBetweenWall(startOffset, locationOffset, place);
                        constructIceSpike(startOffset, locationOffset, place);
                    } else {
                        constructWall(startOffset, locationOffset, vertical, place);
                    }
                    startOffset += 12;
                }
                locationOffset += 12;
            }
        }
    }

    private static void constructBetweenWall(float position1, float position2, Node place) {
        if (!visibleBetweenWall(position1, position2)) {
            return;
        }

        Node betweenWall = new Node();
        Spatial betweenWallMesh = new Geometry("bWall", new Box(1, 5, 1));
        Material betweenWallText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        betweenWallText.setColor("Diffuse", ColorRGBA.Brown);
        betweenWallMesh.setMaterial(betweenWallText);
        betweenWall.attachChild(betweenWallMesh);

        betweenWall.move(WALL_LOCATION.add(position1, 0, position2));

        betweenWall.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(betweenWall);
        place.attachChild(betweenWall);
    }

    private static boolean visibleBetweenWall(float position1, float position2) {
        if ((position1 > 60 && position1 < 96) && (position2 > 60 && position2 < 96)) {
            return false;
        }
        return true;
    }

    private static void constructIceSpike(float position1, float position2, Node place) {
        if (!visibleIceSpike(position1, position2)) {
            return;
        }

        Node iceSpike = new Node();
        Spatial iceSpikeMesh = assetManager.loadModel("Models/IceSpikeFinal.j3o");
        Material iceSpikeText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        iceSpikeText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/IceSpikeTexturetest.png"));
        iceSpikeMesh.setMaterial(iceSpikeText);
        iceSpike.attachChild(iceSpikeMesh);

        iceSpike.setLocalTranslation(ICESPIKE_LOCATION.add(position1, 0, position2));

        iceSpike.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(iceSpike);
        place.attachChild(iceSpike);
    }

    private static boolean visibleIceSpike(float position1, float position2) {
        if (position1 == 0 && position2 == 0) {
            return true;
        } else if (position1 == 60 && position2 == 60) {
            return true;
        } else if (position1 == 60 && position2 == 96) {
            return true;
        } else if (position1 == 96 && position2 == 60) {
            return true;
        } else if (position1 == 96 && position2 == 96) {
            return true;
        } else if (position1 == 156 && position2 == 156) {
            return true;
        }
        return false;
    }

    private static void constructWall(float position1, float position2, int rotate, Node place) {
        if (!visibleWalls(position1, position2)) {
            return;
        }

        Node wall = new Node("wallContainer");
        Spatial insideWallMesh = assetManager.loadModel("Models/WallFinal.j3o");
        Material insideWallText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        if (Utilities.randomChance(.33f)) {
            insideWallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall1of4Final.png"));
        } else if (Utilities.randomChance(.66f)) {
            insideWallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall2of4Final.png"));
        } else {
            insideWallText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/Wall3of4Final.png"));
        }
        insideWallMesh.setMaterial(insideWallText);
        wall.attachChild(insideWallMesh);

        if (rotate == 0) {
            wall.setLocalTranslation(WALL_LOCATION.add(position1, 0, position2));
            wall.rotate(0, FastMath.DEG_TO_RAD * 90, 0);
        } else {
            wall.setLocalTranslation(WALL_LOCATION.add(position2, 0, position1));
        }

        if (Utilities.randomChance(.5f)) {
            wall.rotate(0, FastMath.DEG_TO_RAD * 180, 0);
        }

        wall.addControl(new RigidBodyControl(0));
        bulletAppState.getPhysicsSpace().add(wall);
        place.attachChild(wall);
    }

    private static boolean visibleWalls(float position1, float position2) {
        if (position1 == 0) {
            return true;
        } else if (position1 == 60) {
            if (position2 == 66 || position2 == 90) {
                return true;
            }
        } else if (position1 == 96) {
            if (position2 == 66 || position2 == 90) {
                return true;
            }
        } else if (position1 == 156) {
            return true;
        }

        if ((position1 > 59 && position1 < 97) && (position2 > 59 && position2 < 97)) {
        } else if (position1 == 0 || position2 == 0 || position1 == 156 || position2 == 156) {
        } else if (position1 == 12 && position2 < 12) {
        } else if (position1 == 144 && position2 > 144) {
        } else if (Utilities.randomChance(.5f)) {
            return true;
        }
        return false;
    }

    private static void constructTorch(Node wall, boolean side, int rotate) {
        Node torch = new Node();

        Spatial torchMesh = assetManager.loadModel("Models/torch.j3o");
        Material torchText = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");

        torchText.setTexture("DiffuseMap", assetManager.loadTexture("Textures/torchTexturetest1.png"));
        torchMesh.setMaterial(torchText);
        torch.attachChild(torchMesh);

        if (side && rotate == 0) {
            torch.setLocalTranslation(wall.getLocalTranslation().add(new Vector3f(TORCH_LOCATION_RELATIVE_TO_WALL.getZ(), TORCH_LOCATION_RELATIVE_TO_WALL.getY(), TORCH_LOCATION_RELATIVE_TO_WALL.getX())));
            torch.rotate(FastMath.DEG_TO_RAD * 15, FastMath.DEG_TO_RAD * 90, 0);
            lightTorch(true, rotate, wall.getLocalTranslation().add(0, 4, 0));
        } else if (side && rotate == 1) {
            torch.setLocalTranslation(wall.getLocalTranslation().add(TORCH_LOCATION_RELATIVE_TO_WALL.mult(new Vector3f(-1, 1, 1))));
            torch.rotate(FastMath.DEG_TO_RAD * 15, 0, 0);
            lightTorch(true, rotate, wall.getLocalTranslation().add(0, 4, 0));
        } else if (!side && rotate == 0) {
            torch.setLocalTranslation(wall.getLocalTranslation().add(new Vector3f(TORCH_LOCATION_RELATIVE_TO_WALL.getZ(), TORCH_LOCATION_RELATIVE_TO_WALL.getY(), TORCH_LOCATION_RELATIVE_TO_WALL.getX()).mult(new Vector3f(-1, 1, -1))));
            torch.rotate(FastMath.DEG_TO_RAD * 345, FastMath.DEG_TO_RAD * 90, 0);
            lightTorch(false, rotate, wall.getLocalTranslation().add(0, 4, 0));
        } else {
            torch.setLocalTranslation(wall.getLocalTranslation().add(TORCH_LOCATION_RELATIVE_TO_WALL.mult(new Vector3f(-1, 1, -1))));
            torch.rotate(FastMath.DEG_TO_RAD * 345, 0, 0);
            lightTorch(false, rotate, wall.getLocalTranslation().add(0, 4, 0));
        }

        level.attachChild(torch);
    }

    private static void lightTorch(boolean side, int rotate, Vector3f location) {
        if (counter >= 10) {
            return;
        }

        PointLight light = new PointLight();
        light.setColor(ColorRGBA.White.mult(.2f));
        light.setRadius(20);
        LightNode lightNode = new LightNode("pointLight", light);

        if (side && rotate == 0) {
            light.setPosition(location.add(2.5f, 0, 0));
            lightNode.setLocalTranslation(location.add(2.5f, 0, 0));
        } else if (side && rotate == 1) {
            light.setPosition(location.add(0, 0, 2.5f));
            lightNode.setLocalTranslation(location.add(0, 0, 2.5f));
        } else if (!side && rotate == 0) {
            light.setPosition(location.add(-2.5f, 0, 0));
            lightNode.setLocalTranslation(location.add(-2.5f, 0, 0));
        } else if (!side && rotate == 1) {
            light.setPosition(location.add(0, 0, -2.5f));
            lightNode.setLocalTranslation(location.add(0, 0, -2.5f));
        }

        level.addLight(light);
        level.attachChild(lightNode);
        counter++;
    }
    private static void makeEnemy() {
        model = (Node) assetManager.loadModel("Models/Sinbad/Sinbad.mesh.xml");
        
        AnimControl control = model.getControl(AnimControl.class);
        SkeletonDebugger skeletonDebug = new SkeletonDebugger("skeleton", control.getSkeleton());
        Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat2.getAdditionalRenderState().setWireframe(true);
        mat2.setColor("Color", ColorRGBA.Green);
        
        skeletonDebug.setMaterial(mat2);
        skeletonDebug.setLocalTranslation(model.getLocalTranslation());

        RagdollCollisions listen = new RagdollCollisions();
        
        ragdoll = new KinematicRagdollControl(0.5f);
        setupSinbad(ragdoll);
        ragdoll.addCollisionListener(listen);
        
        model.addControl(ragdoll);
        model.setLocalTranslation(79, 4, 79);
        
        getPhysicsSpace().add(ragdoll);
        level.attachChild(model);
        //ragdoll.setRagdollMode();
        
        model2 = (Node) assetManager.loadModel("Models/Sinbad/Sinbad.mesh.xml");
        
        //AnimControl control = model.getControl(AnimControl.class);
        //SkeletonDebugger skeletonDebug = new SkeletonDebugger("skeleton", control.getSkeleton());
        //Material mat2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat2.getAdditionalRenderState().setWireframe(true);
        mat2.setColor("Color", ColorRGBA.Green);
        
        skeletonDebug.setMaterial(mat2);
        skeletonDebug.setLocalTranslation(model.getLocalTranslation());

        
        
        ragdoll2 = new KinematicRagdollControl(0.5f);
        setupSinbad(ragdoll2);
        ragdoll2.addCollisionListener(listen);
        
        model2.addControl(ragdoll2);
        model2.setLocalTranslation(80, 4, 60);
        
        getPhysicsSpace().add(ragdoll2);
        level.attachChild(model2);
    }
    
     private static void setupSinbad(KinematicRagdollControl ragdoll) {
        ragdoll.addBoneName("Ulna.L");
        ragdoll.addBoneName("Ulna.R");
        ragdoll.addBoneName("Chest");
        ragdoll.addBoneName("Foot.L");
        ragdoll.addBoneName("Foot.R");
        ragdoll.addBoneName("Hand.R");
        ragdoll.addBoneName("Hand.L");
        ragdoll.addBoneName("Neck");
        ragdoll.addBoneName("Root");
        ragdoll.addBoneName("Stomach");
        ragdoll.addBoneName("Waist");
        ragdoll.addBoneName("Humerus.L");
        ragdoll.addBoneName("Humerus.R");
        ragdoll.addBoneName("Thigh.L");
        ragdoll.addBoneName("Thigh.R");
        ragdoll.addBoneName("Calf.L");
        ragdoll.addBoneName("Calf.R");
        ragdoll.addBoneName("Clavicle.L");
        ragdoll.addBoneName("Clavicle.R");
    }
    
    private static PhysicsSpace getPhysicsSpace() {
        return bulletAppState.getPhysicsSpace();
    }

    public void collide(Bone bone, PhysicsCollisionObject object, PhysicsCollisionEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}