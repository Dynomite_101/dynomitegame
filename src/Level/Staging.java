package Level;

import com.jme3.bullet.BulletAppState;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public interface Staging {

    void setup(Node level, BulletAppState bulletAppState);

    Spatial floor(float positionX, float positionZ);

    Spatial partition(float postitionX, float postitionZ);

    Spatial wall(float postitionX, float postitionZ, boolean rotate, boolean breakable);

    Spatial decorationOne(float postitionX, float postitionZ);

    Spatial money(float positionX, float positionZ);
    
    Spatial oneUp(float positionX, float positionZ);
    
    Spatial endDoor(float positionX, float positionZ);
    
    Spatial findKey(float positionX, float positionZ);
}