package enemyAI;

import MainInit.Main;
import MainInit.Utilities;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.bullet.control.BetterCharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import java.util.Collections;
import java.util.List;

public class EnemyAI extends BetterCharacterControl {

    private final float MOVEMENT_INTERVAL = 0.965f;
    private final int TIME_BEFORE_NEXT_ATTACK = 2;
    private final int MOVEMENT_SPEED = 12, ATTACK_SPEED = 20;
    private Utilities.Environment[][] layout;
    private Vector3f minMin;
    private Vector3f maxMin;
    private Vector3f minMax;
    private Vector3f maxMax;
    private float time = 0, stuckTime = 0, attackTime = 0;
    private Utilities.Position direction, lastDirection = Utilities.Position.None;
    private Vector3f lastLocation = location;
    private boolean attacking = false;
    private float tpf = 0;

    public EnemyAI(Utilities.Environment[][] layout, BulletAppState bulletAppState) {
        super(3, 6, 1000);
        walkDirection.zero();
        direction = Utilities.Position.None;
        this.layout = layout;

        bulletAppState.getPhysicsSpace().addCollisionListener(new PhysicsCollisionListener() {
            boolean wall = false, floor = false, attack = false;

            public void collision(PhysicsCollisionEvent event) {

                Spatial node;
                if (event.getNodeA() == (spatial)) {
                    node = event.getNodeB();
                } //else if (event.getNodeB() == (spatial)) {
                //node = event.getNodeA();
                //} 
                else {
                    return;
                }

                if (node == Main.player) {
                    if (attackTime > TIME_BEFORE_NEXT_ATTACK) {
                        Player.Player.loseLife(1);
                        attackTime = 0;
                    }
                }
            }
        });
    }

    public void backOnPath() {
        Vector3f positionX = findNearest(location.x);
        Vector3f positionZ = findNearest(location.z);

        allocateVectors(positionX, positionZ);

        Vector3f closest = findClosest();

        walkDirection.set(closest.subtract(location));
    }

    private Vector3f findNearest(float position) {
        Vector3f location = new Vector3f();

        for (int ctr = 6; ctr <= layout.length * Utilities.WORLD_SCALE; ctr += 12) {
            if (ctr <= position) {
                location.x = ctr;
            } else {
                location.z = ctr;
                break;
            }
        }

        return location;
    }

    private void allocateVectors(Vector3f positionX, Vector3f positionZ) {
        minMin = new Vector3f(positionX.x, location.y, positionZ.x);
        maxMin = new Vector3f(positionX.z, location.y, positionZ.x);
        minMax = new Vector3f(positionX.x, location.y, positionZ.z);
        maxMax = new Vector3f(positionX.z, location.y, positionZ.z);
    }

    private Vector3f findClosest() {
        float distance = location.distance(minMin);
        Vector3f closest = minMin, test = minMin;

        for (int ctr = 0; ctr < 3; ctr++) {
            switch (ctr) {
                case (0):
                    test = maxMin;
                    break;
                case (1):
                    test = minMax;
                    break;
                case (2):
                    test = maxMax;
                    break;
            }

            if (location.distance(test) < distance) {
                distance = location.distance(test);
                closest = test;
            }
        }

        return closest;
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        attackTime += tpf;
        if (location.distance(Player.Player.getPlayerLocation()) < 5 * Utilities.WORLD_SCALE) {
            attacking = true;
        }
        if (!attacking && time >= MOVEMENT_INTERVAL
                && (location.x < Utilities.nearestNumber(location.x, 12, 6) + .05
                && location.x > Utilities.nearestNumber(location.x, 12, 6) - .05)
                && (location.z < Utilities.nearestNumber(location.z, 12, 6) + .05
                && location.z > Utilities.nearestNumber(location.z, 12, 6) - .05)) {
            followPath(tpf);
        }
        if (!attacking && time >= MOVEMENT_INTERVAL) {
            if ((location.x > Utilities.nearestNumber(location.x, 12, 6) + .05
                    || location.x < Utilities.nearestNumber(location.x, 12, 6) - .05)
                    || (location.z > Utilities.nearestNumber(location.z, 12, 6) + .05
                    || location.z < Utilities.nearestNumber(location.z, 12, 6) - .05)) {
                backOnPath();
            }
            time = 0;
        }
        if (attacking) {
            attack();
        }
        if (!attacking && lastLocation == location) {
            if (stuckTime > 2) {
                this.direction = Utilities.oppositeDirection(this.direction);
                followPath(tpf);
                stuckTime = 0;
            }
        }
        if (lastLocation != location) {
            stuckTime = 0;
        }
        attacking = false;
        lastLocation = location;
        time += tpf;
        stuckTime += tpf;
        setViewDirection(walkDirection);
    }

    public void followPath(float tpf) {
        walkDirection.zero();

        List<Utilities.Position> availableDirections = Utilities.directionCheck(location);
        Collections.shuffle(availableDirections);

        try {
            for (Utilities.Position direction : availableDirections) {
                this.direction = direction;
                if (direction != Utilities.oppositeDirection(this.lastDirection)) {
                    this.direction = direction;
                    break;
                }
            }
        } catch (IndexOutOfBoundsException boundsException) {
            PhysicsSpace.getPhysicsSpace().removeAll(spatial);
            spatial.getParent().removeFromParent();
        }

        if (direction == Utilities.Position.Down) {
            walkDirection.addLocal(Vector3f.UNIT_Z);
        } else if (direction == Utilities.Position.Left) {
            walkDirection.addLocal(Vector3f.UNIT_X.negate());
        } else if (direction == Utilities.Position.Up) {
            walkDirection.addLocal(Vector3f.UNIT_Z.negate());
        } else {
            walkDirection.addLocal(Vector3f.UNIT_X);
        }

        walkDirection.normalizeLocal();
        walkDirection.multLocal(MOVEMENT_SPEED);

        setWalkDirection(walkDirection);
        lastDirection = direction;
        time = 0;
    }

    private void attack() {
        Vector3f playerLocation = new Vector3f(Player.Player.getPlayerLocation().x, location.y, Player.Player.getPlayerLocation().z);
        setWalkDirection(playerLocation.subtract(location).normalize().mult(ATTACK_SPEED));
    }
}
