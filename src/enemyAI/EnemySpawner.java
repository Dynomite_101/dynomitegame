package enemyAI;

import MainInit.Factory;
import MainInit.Utilities;
import com.jme3.bullet.BulletAppState;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.Control;

public class EnemySpawner {

    private final int NUMBER_OF_ENEMIES = 3;
    private final Vector3f ENEMY_LOCATION = new Vector3f(0, 2f, 0);
    Utilities.Environment[][] layout;
    private BulletAppState bulletAppState;

    public EnemySpawner(Utilities.Environment[][] layout, BulletAppState bulletAppState) {
        this.layout = layout;
        this.bulletAppState = bulletAppState;
    }

    public Node spawnEnemies() {
        Node enemies = new Node("AllEnemies");
        for (int positionY = layout.length-1; positionY > 0; positionY--) {
            for (int positionX = layout[positionY].length-1; positionX > 0; positionX--) {
                if (layout[positionY][positionX] == Utilities.Environment.EnemySpawn && enemies.getChildren().size() < NUMBER_OF_ENEMIES) {
                    enemies.attachChild(spawn(positionY, positionX));
                }
            }
        }
        return enemies;
    }

    private Spatial spawn(float position1, float position2) {
        Spatial enemy = Factory.constructEnemy();
        enemy.setName("EnemyNode");

        enemy.setLocalTranslation(ENEMY_LOCATION.add(position1 * Utilities.WORLD_SCALE, 0, position2 * Utilities.WORLD_SCALE));

        Control control = new EnemyAI(layout, bulletAppState);
        bulletAppState.getPhysicsSpace().add(control);
        enemy.addControl(control);
        

        return enemy;
    }
}
