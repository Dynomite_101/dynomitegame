package Player;

import Atmosphere.DamageSound;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class Player {

    private static BulletAppState bulletAppState;
    private static CharacterControl playerControl;
    private static Node rootNode;
    private static AssetManager assetManager;
    private static int money = 0;
    private static int lives = 3;
    public static boolean keyBuy = false;
    public static boolean keyKill = false;
    public static boolean keyFind = false;

    public static CharacterControl initPlayer(CharacterControl playerControl, BulletAppState bulletAppState, Node rootNode, AssetManager assetManager) {
        Player.bulletAppState = bulletAppState;
        Player.playerControl = playerControl;
        Player.assetManager = assetManager;
        Player.rootNode = rootNode;
        control();

        return Player.playerControl;
    }

    private static void control() {
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(2, 6, 1);
        playerControl = new CharacterControl(capsuleShape, 0.01f);
        playerControl.setJumpSpeed(20);
        playerControl.setFallSpeed(30);
        playerControl.setGravity(30);
        playerControl.setPhysicsLocation(new Vector3f(-10, 10, 20));            
        
        
        bulletAppState.getPhysicsSpace().add(playerControl);
    }

    public static CharacterControl getPlayer() {
        return playerControl;
    }

    public static Vector3f getPlayerLocation() {
        return playerControl.getPhysicsLocation();
    }

    public Player() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static void addMoney(int money) {
        Player.money += money;
    }

    public static void spendMoney(int money) {
        Player.money -= money;
    }

    public static int getMoney() {
        return money;
    }

    public static void addLife(int life) {
        Player.lives += life;
    }

    public static void loseLife(int life) {
        Player.lives -= life;
        if (Player.getLives() > 0) {
            DamageSound damageSound = new DamageSound(rootNode, assetManager);
        }
    }

    public static int getLives() {
        return lives;
    }
}
