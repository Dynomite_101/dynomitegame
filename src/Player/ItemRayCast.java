package Player;

import MainInit.ScreenControl;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;

public class ItemRayCast{
    
    private static float castTimer;
    private static float castCoolDown = 1f;
    private static Camera cam;
    private static Node items;
    ScreenControl s;
    
    public static void getCam(Camera cam)
    {
        ItemRayCast.cam = cam;
    }
    
    public static void getNode(Node items)
    {
        ItemRayCast.items = items;
    }
   
    
    public static String autoCast()
    {
        String hit = " ";

        CollisionResults results = new CollisionResults();

        Ray ray = new Ray(cam.getLocation(), cam.getDirection());

        items.collideWith(ray, results);

        for (int i = 0; i < results.size(); i++) {

           if(i==0){
               hit = results.getCollision(i).getGeometry().getName();
               return hit;
           }  
        }
        
        return hit;
    }
}

